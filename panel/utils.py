import datetime
import pyspark.sql.functions as F
import pyspark.sql.types as T


# Once we get to Spark 2.4 we can ditch this and use F.array_intersect
@F.udf(T.ArrayType(T.IntegerType()))
def array_intersect(array1, array2):
    return list(set(array1) & set(array2))


# Once we get to Spark 2.4 we can ditch this and use F.array_distinct
@F.udf(T.ArrayType(T.LongType()))
def array_distinct(array1, array2):
    return list(set(array1) - set(array2))


def stash_results(func):
    """
    Decorator function so we can automatically save the output of our dag
    functions.
    """
    def wrapper_spark_decorator(*args, **kwargs):
        data = func(*args, **kwargs) # run the function/query
        config = args[-1] 
        return save(data, config["output_name"]) # stash and return metadata
    return wrapper_spark_decorator


def clone_dataframe(df):
    """
    Accepts a `pyspark.sql.dataframe.DataFrame` and returns a clone that can
    be used to circumvent Spark reference errors. Sometimes we need to do this
    when we are merging a child dataframe that was derived directly from a 
    parent dataframe. Renaming the columns (even though we are giving them the
    same names) will force Spark to create a new reference for the child with
    absolutely no computational overhead.
    """
    for col in df.columns:
        df = df.withColumnRenamed(col, col)
    return df


def time_shift(date_string, offset_days):
    """
    Apply an offset to a date string. If offset is positive we will shift 
    the date forward and if negative we will shift backward.

    Args:
    -----
    date_string: `str`, required
        Date string in YYYY-mm-dd format
    offset_days: `int`, required
        Number of days to shift the date forward (+) or backward (-)

    Returns:
    --------
    A date string in YYYYmmdd format
    """
    date = datetime.datetime.strptime(date_string, "%Y-%m-%d")
    date = date + datetime.timedelta(days=offset_days)
    return date.strftime("%Y%m%d")


def set_column_value(df, col1, col2, output_name=None):
    """
    Utility function to set a final value for a set of two columns for the 
    case when the first column is NULL.
    
    Args:
    -----
    df: `pyspark.sql.dataframe.DataFrame`, required
        A pyspark dataframe
    col1: `str`, required
        A name corresponding to the column with the NULL values to be replaced
    col2: `str`, required
        A name corresponding to the column with the replacement values in the
        event we encounter a NULL in the first column
    output_name: `str`, required
        Name of the output column
    
    Returns:
    --------
    A spark dataframe containing with the new column values.
    """
    output_name = col1 if output_name is None else output_name
    
    return df.withColumn(
        output_name,
        F.when(
            F.col(col1).isNull(),
            F.col(col2)
        ).otherwise(
            F.col(col1)
        )
    )


def cast_timestamp(df, cols):
    """
    Converts columns to pyspark timestamp format
    """
    for col in cols:
        df = df.withColumn(col, F.col(col).cast("timestamp"))
    return df


def subtract_timestamps(col1, col2):
    return F.unix_timestamp(col1) - F.unix_timestamp(col2)


def timestamp_delta(col, n_seconds=1):
    delta = F.unix_timestamp(col) + n_seconds
    return F.to_timestamp(delta)

def calculate_duration(start_time, end_time):
    total_seconds = F.unix_timestamp(
        F.col(end_time)) - F.unix_timestamp(
        F.col(start_time)
    )
    return total_seconds / 3600
    