import pyspark.sql.functions as F
from pyspark.sql.window import Window
from pyspark.sql.functions import date_format


def add_race_features(spark, df):
    """
    Calculate event durations and dayparts on a spark dataframe
    
    Args:
    -----
    df: `pyspark.sql.dataframe.DataFrame`, required
        A data frame of tuning events; the output of the `pull_data` function
    
    Returns:
    --------
    A `pyspark.sql.dataframe.DataFrame` containing formatted tuning events
    """

    df_with_race = spark.sql(
        """SELECT DISTINCT
            original_location_media_consumer_key AS location_media_consumer_key,
            is_asian_residence_flag,
            is_african_american_residence_flag,
            CASE WHEN head_of_house_ethnic_identity_code = 1 THEN 'NON-HISPANIC'
                WHEN head_of_house_ethnic_identity_code = 2 AND language_classification_code = 1 THEN 'SPANISH_ONLY'
                WHEN head_of_house_ethnic_identity_code = 2 AND language_classification_code = 2 THEN 'ENGLISH_ONLY'
                WHEN head_of_house_ethnic_identity_code = 2 AND language_classification_code = 3 THEN 'MOSTLY_SPANISH'
                WHEN head_of_house_ethnic_identity_code = 2 AND language_classification_code = 4 THEN 'MOSTLY_ENGLISH'
                WHEN head_of_house_ethnic_identity_code = 2 AND language_classification_code = 5 THEN 'SPANISH_ENGLISH_EQUAL' END AS language_class
            
        FROM 
            tam_npm_mch_tv_prod.metered_tv_household_location_dimension  a
        
            """
    )

    df = df.join(df_with_race, on=["location_media_consumer_key"], how="inner")

    return df


def add_number_of_sets(spark, df):
    """
    Calculate event durations and dayparts on a spark dataframe
    
    Args:
    -----
    df: `pyspark.sql.dataframe.DataFrame`, required
        A data frame of tuning events; the output of the `pull_data` function
    
    Returns:
    --------
    A `pyspark.sql.dataframe.DataFrame` containing formatted tuning events
    """

    device_df = spark.sql(
        """ SELECT
            DISTINCT 
                household_media_consumer_id,
                source_installed_location_number,
                source_media_device_number,
                media_device_type_code,
                media_device_status_code,
                media_device_is_metered_flag,
                is_tv_set_flag
            FROM
                tam_npm_mch_tv_prod.metered_tv_media_device_tuner
            WHERE TRUE
                AND media_device_is_metered_flag = 'Y'
            """
    )

    device_df = (
        device_df.groupBy(["household_media_consumer_id"])
        .count()
        .withColumnRenamed("count", "number_of_sets")
    )

    df = df.join(device_df, on=["household_media_consumer_id"], how="inner")

    return df


def make_demo_bucket(df):
    """
    Calculate event durations and dayparts on a spark dataframe
    
    Args:
    -----
    df: `pyspark.sql.dataframe.DataFrame`, required
        A data frame of tuning events; the output of the `pull_data` function
    
    Returns:
    --------
    A `pyspark.sql.dataframe.DataFrame` containing formatted tuning events
    """

    condition = (
        F.when(F.col("age").between(2, 5), F.lit("P2-5"))
        .when(F.col("age").between(6, 11), F.lit("P6-11"))
        .when(
            (F.col("age").between(12, 17)) & (F.col("gender") == "M"), F.lit("M12-17")
        )
        .when(
            (F.col("age").between(12, 17)) & (F.col("gender") == "F"), F.lit("F12-17")
        )
        .when(
            (F.col("age").between(18, 34)) & (F.col("gender") == "M"), F.lit("M18-34")
        )
        .when(
            (F.col("age").between(18, 34)) & (F.col("gender") == "F"), F.lit("F18-34")
        )
        .when(
            (F.col("age").between(35, 54)) & (F.col("gender") == "M"), F.lit("M35-54")
        )
        .when(
            (F.col("age").between(35, 54)) & (F.col("gender") == "F"), F.lit("F35-54")
        )
        .when((F.col("age") >= 55) & (F.col("gender") == "M"), F.lit("M55+"))
        .when((F.col("age") >= 55) & (F.col("gender") == "F"), F.lit("F55+"))
        .otherwise(F.lit("under 2"))
    )

    df = df.withColumn("demo_10way", condition)

    return df


def relative_peson_type(spark, df, final):
    """
    Calculate event durations and dayparts on a spark dataframe
    
    Args:
    -----
    df: `pyspark.sql.dataframe.DataFrame`, required
        A data frame of tuning events; the output of the `pull_data` function
    
    Returns:
    --------
    A `pyspark.sql.dataframe.DataFrame` containing formatted tuning events
    """

    df = df.withColumn(
        "kid_adult_cat",
        F.when((F.col("gender") == "M") & (F.col("age") >= 18), "adult_male")
        .when((F.col("gender") == "F") & (F.col("age") >= 18), "adult_female")
        .when((F.col("age") < 18), "kids"),
    )

    df = df.withColumn(
        "age_ranked",
        F.rank().over(
            Window.partitionBy(
                "intab_period_id", "household_media_consumer_id", "kid_adult_cat"
            ).orderBy(F.col("age").desc())
        ),
    )

    df = df.withColumn(
        "person_type",
        F.when(
            (F.col("kid_adult_cat") == "adult_male") & (F.col("age_ranked") == 1),
            "Oldest/Only Male Adult",
        )
        .when(
            (F.col("kid_adult_cat") == "adult_male") & (F.col("age_ranked") != 1),
            "Not Oldest Male Adult",
        )
        .when(
            (F.col("kid_adult_cat") == "adult_female") & (F.col("age_ranked") == 1),
            "Oldest/Only Female Adult",
        )
        .when(
            (F.col("kid_adult_cat") == "adult_female") & (F.col("age_ranked") != 1),
            "Not Oldest Female Adult",
        )
        .when(
            (F.col("kid_adult_cat") == "kids") & (F.col("age_ranked") == 1),
            "Oldest/Only Kid",
        )
        .when(
            (F.col("kid_adult_cat") == "kids") & (F.col("age_ranked") != 1),
            "Not Oldest Kidd",
        ),
    )

    df = df.select(
        [
            "intab_period_id",
            "household_media_consumer_id",
            "location_media_consumer_id",
            "person_media_consumer_id",
            "person_type",
        ]
    )

    final = final.join(
        df,
        on=[
            "intab_period_id",
            "household_media_consumer_id",
            "location_media_consumer_id",
            "person_media_consumer_id",
        ],
        how="inner",
    )

    return final


def add_person_weight(spark, df):

    """
    Calculate event durations and dayparts on a spark dataframe
    
    Args:
    -----
    df: `pyspark.sql.dataframe.DataFrame`, required
        A data frame of tuning events; the output of the `pull_data` function
    
    Returns:
    --------
    A `pyspark.sql.dataframe.DataFrame` containing formatted tuning events
    """
    df.createOrReplaceTempView("df_temp")

    weight_df = spark.sql(
        """SELECT
            df.*,
            w.person_weight
        FROM 
            df_temp df
        JOIN
            tam_npm_mch_tv_prod.metered_tv_household_location_person_intab_and_weight w 
        ON
            df.intab_period_id = w.intab_period_id
            AND df.household_media_consumer_id = w.household_media_consumer_id
            AND df.location_media_consumer_id = w.location_media_consumer_id
            AND df.person_media_consumer_id = w.person_media_consumer_id
            
        WHERE
            sample_collection_method_key = 7 AND media_device_category_code = 1
         
    """
    )

    return weight_df


def add_hut_ind(spark, df):
    """
    Calculate event durations and dayparts on a spark dataframe
    
    Args:
    -----
    df: `pyspark.sql.dataframe.DataFrame`, required
        A data frame of tuning events; the output of the `pull_data` function
    
    Returns:
    --------
    A `pyspark.sql.dataframe.DataFrame` containing formatted tuning events
    """

    hut_station_path = "s3a://us-east-1-nlsn-w-tam-ltamingestion-gtam-prod-01/data/panel_ingestion/external/AUDIENCE_IMPUTATION_NON_HUT_STATIONS/AUDIENCE_IMPUTATION_NON_HUT_STATIONS.txt"

    hut_station_file = spark.read.text(hut_station_path).collect()

    non_hut = [i.value.split(",")[0] for i in hut_station_file]

    df = df.withColumn(
        "HUT",
        F.when(F.col("metered_tv_media_credit_id").isin(non_hut), "NON-HUT").otherwise(
            "HUT"
        ),
    )

    return df


def make_qh(spark, df, col_name, new_col_name):
    """
    Calculate event durations and dayparts on a spark dataframe
    
    Args:
    -----
    df: `pyspark.sql.dataframe.DataFrame`, required
        A data frame of tuning events; the output of the `pull_data` function
    
    Returns:
    --------
    A `pyspark.sql.dataframe.DataFrame` containing formatted tuning events
    """

    df = df.withColumn(
        "start_minute_of_day",
        F.hour(F.col(col_name)) * 60 + F.floor(F.minute(F.col(col_name)) / 15) * 15,
    )

    df = df.withColumn(
        "start_minute_of_day",
        F.when(
            F.col("start_minute_of_day") < 300, F.col("start_minute_of_day") + 1440
        ).otherwise(F.col("start_minute_of_day")),
    )

    qh_file_s3_path = "/FileStore/tables/quarter_hour_file.csv"
    qh_reference = (
        spark.read.load(
            qh_file_s3_path, format="csv", inferSchema="true", header="true"
        )
        .withColumnRenamed("FIRST_MAM_OF_QUARTER_HOUR", "start_minute_of_day")
        .withColumnRenamed("CALENDAR_DAY_OF_WEEK_NUMBER", "week_number")
    )

    df = df.join(
        qh_reference, on=["start_minute_of_day", "week_number"], how="inner"
    ).select(df["*"], qh_reference["QUARTER_HOUR_OF_WEEK_KEY"].alias(new_col_name))

    return df.drop('start_minute_of_day')


def make_daypart(spark, df, reference_file_path, daypart_col_name):
    """
    Calculate event durations and dayparts on a spark dataframe
    
    Args:
    -----
    df: `pyspark.sql.dataframe.DataFrame`, required
        A data frame of tuning events; the output of the `pull_data` function
    
    Returns:
    --------
    A `pyspark.sql.dataframe.DataFrame` containing formatted tuning events
    """

    df = df.withColumn(
        "event_start_local", date_format("event_start_local", "HH:mm")
    ).withColumn("event_end_local", date_format("event_end_local", "HH:mm"))
    df.createOrReplaceTempView("df")

    daypart_reference = spark.read.load(
        reference_file_path, format="csv", inferSchema="true", header="true"
    )

    daypart_reference = daypart_reference.withColumn(
        "start_hour", date_format("start_hour", "HH:mm")
    ).withColumn("end_hour", date_format("end_hour", "HH:mm"))

    daypart_reference.createOrReplaceTempView("daypart_reference")

    df = spark.sql(
        """
    SELECT
        tuning.*,
        ref.definition as {daypart_col_name}
    FROM
        df tuning
    JOIN 
        daypart_reference ref
    ON
        tuning.week_number between ref.start_day and ref.end_day
        AND tuning.event_end_local   >= ref.start_hour
        AND tuning.event_start_local <= ref.end_hour
    """.format(
            daypart_col_name=daypart_col_name
        )
    )

    return df


def add_room_location(spark, df):

    """
    Calculate event durations and dayparts on a spark dataframe
    
    Args:
    -----
    df: `pyspark.sql.dataframe.DataFrame`, required
        A data frame of tuning events; the output of the `pull_data` function
    
    Returns:
    --------
    A `pyspark.sql.dataframe.DataFrame` containing formatted tuning events
    """

    df_with_room = spark.sql(
        """SELECT 
            *,
            CASE 
                WHEN MEDIA_DEVICE_LOCATION_CATEGORY_CODE IN ('MB') THEN 1
                WHEN MEDIA_DEVICE_LOCATION_CATEGORY_CODE IN ('BR') THEN 2 
                ELSE 3
            END AS BYTEINT
        FROM 
            tam_npm_mch_tv_prod.metered_tv_media_device_tuner 
        """
    )

    df = df.join(
        df_with_room,
        on=[
            "household_media_consumer_id",
            "location_media_consumer_id",
            "media_device_key",
        ],
        how="left",
    ).select(df["*"], df_with_room["BYTEINT"])

    return df


def add_smoothing_factor(df):
    """
    Calculate event durations and dayparts on a spark dataframe
    
    Args:
    -----
    df: `pyspark.sql.dataframe.DataFrame`, required
        A data frame of tuning events; the output of the `pull_data` function
    
    Returns:
    --------
    A `pyspark.sql.dataframe.DataFrame` containing formatted tuning events
    """

    df = df.withColumn(
        "smoothing_factor",
        F.when(
            F.col("intab_period_id") == F.col("viewed_collection_period_id"), "3.465"
        ).otherwise("0.005833"),
    )

    return df


def add_collapsed_genre(spark, df):
    """
    Calculate event durations and dayparts on a spark dataframe
    
    Args:
    -----
    df: `pyspark.sql.dataframe.DataFrame`, required
        A data frame of tuning events; the output of the `pull_data` function
    
    Returns:
    --------
    A `pyspark.sql.dataframe.DataFrame` containing formatted tuning events
    """

    f = "s3://us-east-1-nlsn-w-tam-ltamingestion-gtam-prod-01/data/panel_ingestion/external/AUDIENCE_IMPUTATION_TV_MEDIA_CREDIT_GENRE/effectivestartdate=20200401"
    names = [
        "METERED_TV_CREDIT_STATION_CODE",
        "TV_MEDIA_CREDIT_DESC",
        "TV_MEDIA_CREDIT_SHORT_DESC",
        "INTERNAL_CALL_LETTERS",
        "DISTRIBUTOR_TYPE_CODE",
        "MARKET_OF_ORIGIN_ID",
        "NATIONAL_SERVICE_LEGACY_SERVICE_CODE",
        "GENRE_TYPE_CODE",
        "GENRE_TYPE_DESC",
        "GENRE_ALPHA_CODE",
        "GENRE_DESC",
        "CAPPING_GENRE_ID",
        "CAPPING_GENRE_DESC",
        "LOCATION_TYPE",
        "LOCATION_CODE",
        "EFFECTIVE_START_DATE",
        "EFFECTIVE_END_DATE",
        "RELEASED_FOR_PROCESSING_FLAG",
        "PRIORITY_INDICATOR",
        "AI_STATION_IND",
        "UNIQUE_ID",
        "LOAD_ID",
        "CREATION_DATETIME",
        "LAST_UPDATED_DATETIME",
        "LAST_UPDATED_BY_LOAD_ID",
    ]
    station_genre_mapping = (
        spark.read.option("delimiter", ",").option("inferSchema", "true").csv(f)
    )

    for i, name in enumerate(names):
        station_genre_mapping = station_genre_mapping.withColumnRenamed(
            "_c" + str(i), name
        )

    station_genre_mapping = station_genre_mapping.filter("LOCATION_CODE = 11")

    f = "s3a://us-east-1-nlsn-w-tam-ltamingestion-gtam-prod-01/data/panel_ingestion/external/local_va_with_names/AUDIENCE_IMPUTATION_COLLAPSED_AFFILIATE_GENRE_MAP/"
    names = [
        "AFFILIATE_GENRE_ID",
        "COLLAPSED_AFFILIATE_GENRE_ID",
        "GENRE_TYPE_CODE",
        "GENRE_ALPHA_CODE",
        "AFFILIATE_GENRE",
        "COLLAPSED_AFFILIATE_GENRE",
        "RELEASED_FOR_PROCESSING_FLAG",
        "EFFECTIVE_START_DATE",
        "EFFECTIVE_END_DATE",
    ]
    collapsed_genre_table = (
        spark.read.option("delimiter", ",").option("inferSchema", "true").csv(f)
    )
    for i, name in enumerate(names):
        collapsed_genre_table = collapsed_genre_table.withColumnRenamed(
            "_c" + str(i), name
        )

    genre_table = station_genre_mapping.join(
        collapsed_genre_table, on=["genre_type_code", "genre_alpha_code"], how="left"
    )
    df = df.join(genre_table, on=["METERED_TV_CREDIT_STATION_CODE"], how="inner").select(
        df["*"], genre_table["COLLAPSED_AFFILIATE_GENRE"]
    )

    return df


def add_broader_program_name(spark, df):

    """
    Calculate event durations and dayparts on a spark dataframe
    
    Args:
    -----
    df: `pyspark.sql.dataframe.DataFrame`, required
        A data frame of tuning events; the output of the `pull_data` function
    
    Returns:
    --------
    A `pyspark.sql.dataframe.DataFrame` containing formatted tuning events
    """

    f = "s3a://us-east-1-nlsn-w-tam-ltamingestion-gtam-prod-01/data/panel_ingestion/external/local_va_with_names/AUDIENCE_IMPUTATION_PROGRAM_TYPE_MAPPING/"
    names = [
        "program_type_id",
        "program_expanded_type_code",
        "program_type_desc",
        "parent_program_id",
        "parent_program_code",
        "parent_program_desc",
        "effective_start_date",
        "effective_end_date",
        "released_for_processing_flag",
        "creation_date_time",
    ]
    parent_program_table = (
        spark.read.option("delimiter", "~").option("inferSchema", "true").csv(f)
    )
    for i, name in enumerate(names):
        parent_program_table = parent_program_table.withColumnRenamed(
            "_c" + str(i), name
        )

    df = df.join(
        parent_program_table, on=["program_expanded_type_code"], how="inner"
    ).select(df["*"], parent_program_table["parent_program_code"])

    return df
