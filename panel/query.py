import pyspark.sql.functions as F

from .utils import clone_dataframe, calculate_duration
from .genre.conditions import collapse_genre

from .intab import household
from .intab import intab
from .intab import demo

from .tuning import provider
from .tuning import event
from .tuning import viewing
from .tuning import helpers

from .genre import conditions
from .genre import genre


def make_household_intab(
    spark, 
    start_date, 
    end_date, 
    collection_method="hybrid"
):

    intab_df = intab.get_household_intab(
        spark, 
        start_date, 
        end_date, 
        collection_method
    )
    
    return intab_df


def make_person_intab(spark, start_date, end_date):
    intab_df = intab.get_person_intab(spark, start_date, end_date)
    return intab_df


def make_dma_ids(spark):
    dma_df = household.get_dma_ids(spark)
    return dma_df


def make_demos(spark, household_intab, person_intab, end_date):
    """
    Orchestration function to pull demographic information for panelists in the 
    NPM panel.  This function will return a dataframe of panelist IDs and their
    respective ages and genders data as of the latest intab date.
    
    Args
    -----
    spark: `pyspark.sql.session.SparkSession`, required
        A pyspark session we can use to query the MDL
    household_intab:
    person_intab:
    end_date: `str`, required
        Date string in YYYY-mm-dd format defining the end of the range which
        will be used to calculate panelist ages
    
    
    Returns
    -------
    A pyspark dataframe containing demographic information for all known 
    panelists
    """
    
    demo_df = demo.demo_pull(spark, end_date)
    demo_df = demo.make_demos(demo_df)
    
    intab_df = household_intab.drop("location_media_consumer_key").join( # Combine intab tables
        person_intab.drop("week_number"),
        on=[
            "intab_period_id",
            "household_media_consumer_id",
            "location_media_consumer_id"
        ],
        how="inner"
    )
    
    demo_data = demo_df.join(
        intab_df,
        on=[
            "household_media_consumer_id",
            "person_media_consumer_id"
        ],
        how="inner"
    )
    
    return demo_data
    

def make_events(
    spark, 
    household_intab,
    start_date, 
    end_date,
):
    """
    Orchestration function to pull Nielsen tuning data for intab panelists and
    households. Also creates demos, calculates dayparts, and brings in VOD
    provider information.
    
    This is meant to be the canonical function to create tuning data and 
    absolutely under no circumstances will we ever return viewer assigned 
    panelists (a.k.a. vampires). As such this function cannot be used to pull
    set meter data or diary data.
    
    Args:
    -----
    spark: `pyspark.sql.session.SparkSession`, required
        A pyspark session we can use to query the MDL
    household_intab: `pyspark.sql.dateframe.DataFrame`, required
    collection_method: `string`, optional
        If set to `hybrid` we will include all tuning events originating from 
        people meters; if `npm` we will include only tuning events from NPM
        households; if `lpm` we will only include events from LPM houses; and
        if `tvpc` we will only include events from the TV/PC panel
        
    Returns:
    --------
    A pyspark dataframe containing all of the device-level tuning events across 
    the given time period
    """

    event_df = event.event_pull(spark, household_intab, start_date, end_date)
    event_df = event.make_dayparts(event_df)
     
    provider_df = provider.provider_pull(
        spark, 
        start_date, 
        end_date,
    ).drop("calendar_date")
    
    tuning = event_df.join(
        provider_df,
        on=[
            "intab_period_id",
            "household_media_consumer_id",
            "location_media_consumer_id"
        ],
        how="left"
    ).drop(
        "week_number"
    )
    
    return tuning
    

def make_viewing(spark, demo_df, start_date, end_date):
    """
    Orchestration function to pull person-level tuning events from the MDL. 
    This is almost identical to `events_pull` except we do not immediately 
    bring in the device data to narrow down the results of this query. We 
    will do this once in the `make_training_data` step.

    Args:
    -----
    spark: `pyspark.sql.session.SparkSession`, required
        A pyspark session we can use to query the MDL
    start_date: `str`, required
        Date string in YYYY-mm-dd format defining the start of the range
    end_date: `str`, required
        Date string in YYYY-mm-dd format defining the end of the range
        
    Returns:
    --------
    A pyspark dataframe containing all the person-level tuning events across
    the given time period
    """
    
    data = viewing.viewing_pull(
        spark, 
        demo_df,
        start_date,
        end_date
    )
    
    return data

    
def make_device(
    spark, 
    query_string,
    id_string, 
    start_date, 
    end_date
):
    """
    Orchestration function to process device data from loading,
    formatting and cleaning, exclusion on through to final aggregation.
    
        Args:
    -----
    spark: `pyspark.sql.session.SparkSession`, required
        A Pypsark session we can use to convert the parquet files to 
        `pyspark.sql.dataframe.DataFrame` objects
    query_string: `str`, required
        A string we can use to match on the `query` parameter of files
        residing in the datacache
    id_string: `str`, required
        A string we can use to match on the `run_id` parameter of files in 
        the datacache (`run_id` is usually a timestamp so usually it is 
        sufficient to pass a date string in YYYY-mm-dd format)
        
    Returns:
    --------
    A pyspark dataframe containing the cleaned device data (these are static
    files)
    """

    from .device import device # TODO: query directly from the MDL
    
    # Also performs formatting and cleaning steps
    raw_device = device.device_pull(
        spark, 
        query_string, 
        id_string, 
        start_date, 
        end_date
    )
    
    # Calculate exclusion table
    exclusion = clone_dataframe(device.make_exclusion(raw_device))
    
    # Exclude flagged devices
    device_excluded = raw_device.join( 
        exclusion,
        on=exclusion.columns,
        how="left_anti" # everything in this df that is not in that df
    )
    
    # Rename a few columns
    device_df = device.aggregate_device_data(
        device_excluded
    ).withColumnRenamed(
        "InstalledHousingUnitNum",
        "source_installed_location_number"
    ).withColumnRenamed(
        "SiteUnitId",
        "source_media_device_number"
    )
    
    return device_df


def make_coded_events(spark_session, event_df, start_date, end_date):
    """
    Orchestration function to create a lookup table for NPM genres. The lookup
    is subset such that only keys from our tuning events table will appear in 
    the final output. If a tuning event does not appear in this lookup, it 
    means that Nielsen does not have any coding information for the 
    corresponding program.

    Args:
    -----
    spark: `pyspark.sql.session.SparkSession`, required
        A pyspark session we can use to query the MDL
    start_date: `str`, required
        Date string in YYYY-mm-dd format defining the start of the range
    end_date: `str`, required
        Date string in YYYY-mm-dd format defining the end of the range
        
    Returns:
    --------
    A pyspark dataframe containing all of the coded events for programs that 
    are reported nationally
    """
   
    info_df = genre.genre_pull(spark_session)
    coded_df = genre.get_coded_events(spark_session, start_date, end_date)
    
    coded_event = event_df.join(
        coded_df,
        on=[
            "media_engagement_event_key",
            "household_media_consumer_id",
            "location_media_consumer_id",
            "media_device_id",
            "source_installed_location_number",
            "source_media_device_number",
            "metered_tv_credit_station_code",
            "metered_tv_media_credit_id"
        ],
        how="inner"
    ).join(
        info_df,
        on="report_originator_lineup_key",
        how="inner"
    )
    
    return coded_event


def make_event_genre(spark_session, coded_event_df):
    """
    Orchestration function to deduplicate and cleanse coded tuning events.
    When merging up with the `program_coded` tables in the MDL, we observe a 
    significant amount of overlapping tuning events. Either people will be 
    credited to two different programs simultaneously, or they will be 
    credited to the same program under multiple, overlapping time frames. 
    
    At this time, we suspect a lot of the confusion surrounding these edge 
    cases can be sorted out using the `distribution_event_verified_state_code` 
    field, but unless we have a better data dictionary all we can use it for 
    at this point is as a guide in our sorting and deduplication routine.    
    
    Args:
    -----
    spark: `pyspark.sql.session.SparkSession`, required
        A pyspark session we can use to query the MDL
    coded_event_df: `pyspark.sql.dataframe.DataFrame`, required
        A spark dataframe containing coded tuning events at program-level, the
        output of the `make_coded_events` function above
        
    Returns:
    --------
    A deduplicated genre lookup table which can be used to add genres to 
    device-level tuning events
    """
    
    flags = clone_dataframe(
        genre.make_flags(coded_event_df)
    )
    
    genre_lookup = coded_event_df.join(
        flags.filter(
            ~F.col("is_duplicated")
        ),
        on=[
            "media_engagement_event_key",
            "program_coded_media_engagement_event_key",
            "report_originator_lineup_key"
        ],
        how="inner"
    ).select(
        "media_engagement_event_key",
        "program_coded_media_engagement_event_key",
        "report_originator_lineup_key",
        "media_device_key" ,
        "program_name",
        "genre",
        "program_expanded_type_code",
        "is_live",
        "lineup_type_code",
        "reprocessing_type_code",
        "feed_pattern_code",
        "event_verified_code",
        "report_start_time_ny",
        "report_end_time_ny",
        "tuning_credit_start_utc",
        "tuning_credit_end_utc",
        "tuning_start_local",
        "tuning_end_local",
        "tuning_start_utc", 
        "tuning_end_utc",
        "time_shift_code",
        "time_shift_seconds"
    )
    
    return genre_lookup
    
    
def make_tuning_events(
    event_df,
    genre_df
):
                
    coded_tuning = event_df.join( # merge genre info if present
        clone_dataframe(genre_df),
        on="media_engagement_event_key",
        how="left"
    )
    
    # Fill in any gaps
    filled_tuning = event.fill_gaps(coded_tuning)
    
    # Null out the gap rows that were added
    for col in [
        "program_coded_media_engagement_event_key",
        "report_originator_lineup_key",
        "program_name",
        "genre",
        "is_live",
        "lineup_type_code",
        "reprocessing_type_code",
        "feed_pattern_code",
        "event_verified_code",
        "report_start_time_ny",
        "report_end_time_ny"
    ]:
        filled_tuning = filled_tuning.withColumn(
            col, 
            F.when(
                F.col("gap")=="keep",
                F.col(col)
            ).otherwise(
                F.lit(None)
            )
        )
    
    tuning_data = filled_tuning.withColumn( 
        "genre",
        F.when( # Convert NULL genres to ALL
            F.col("genre").isNull(),
            F.lit("ALL")  
        ).otherwise(
            F.col("genre")
        )
    )
    
    return tuning_data


def make_panel_events(
    dma_df,
    tuning_df, 
    viewing_df
):
    
    # 2. ADD PERSON VIEWING
    tuning_with_viewing = viewing.merge_tuning_with_viewing(
        tuning_df,
        viewing_df.drop(
            "household_type",
            "is_tvpc"
        )
    ).join(
        dma_df,
        on="market_id",
        how="left"
    )
    
    # 3. CREATE FINAL TIMESTAMPS
    reconciled_twv = helpers.reconcile_timestamps(tuning_with_viewing)
    panel_events = helpers.swap_timestamps(reconciled_twv)
    
    return panel_events


def align_panel_data(
    demo_df,
    panel_df,
    device_df=None,
    remove_single_person_homes=True,
    viewed_events_only=False
):
    """
    Orchestration function to create the final tuning/viewing table. Here we
    merge in all of the household features, the device information, program
    and genre information, as well as the person-level viewing information. We
    also collapse down to the VAM genre and set final viewing times and 
    durations.
    
    Args:
    -----
    spark: `pyspark.sql.session.SparkSession`, required
        A pyspark session we can use to query the MDL
    tuning_df: `pyspark.sql.dataframe.DataFrame`, required
        A spark dataframe containing device-level tuning events, the output of
        the `make_tuning` function above
    viewing_df: `pyspark.sql.dataframe.DataFrame`, required
        A spark dataframe containing person-level tuning events, the output of 
        the `make_viewing` function above
    genre_df: `pyspark.sql.dataframe.DataFrame`, required
        A spark dataframe containing the coded program-level events, the output
        of the `make_event_genre` function above
    device_df: `pyspark.sql.dataframe.DataFrame`, optional
        An optional spark dataframe containing device information at household
        level, the output of the `make_device` function above
    viewed_events_only: `boolean`, optional
        A flag to indicate whether we should return all tuning events or only 
        those events where a person was viewing.
    remove_single_person_homes: `boolean`, optional
        If set to `True`, we will only keep households for which we have device
        information and household size greater than 1, otherwise if `False` we 
        will keep every household regardless of device status or size
        
    Returns:
    --------
    A spark dataframe containing fully coded and deduplicated Nielsen tuning events
    """
    
    how = "inner" if remove_single_person_homes else "left"
    
    household_counts = household.aggregate_household_counts(demo_df)
    household_features = household.add_household_features(household_counts)
    
    # 1. ADD HOUSEHOLD FEATURES
    panel_data = panel_df.join( # merge household sizes, etc
        household_features,
        on=[
            "intab_period_id",
            "household_media_consumer_id",
            "location_media_consumer_id"
        ],
        how=how
    )
    
    # 2. ADD DEVICE DATA (IF WE HAVE IT)
    if device_df is not None:
        panel_data = panel_data.join( # merge device information
            device_df,
            on=[
                "source_installed_location_number",
                "source_media_device_number"
            ],
            how=how
        )
        
    # 3. ALIGN TUNING SCHEDULES
    consistent_tuning = helpers.make_tuning_times_consistent(panel_data)
    reconciled_tuning = helpers.reconcile_timestamps(consistent_tuning)
    swapped_tuning = helpers.swap_timestamps(reconciled_tuning)

    # 4. COPY VIEWING ACROSS THE HOUSEHOLD (only if we need to)
    demo_data = demo_df.select(
            "intab_period_id",
            "household_media_consumer_id",
            "location_media_consumer_id",
            "person_media_consumer_id",
            "age",
            "gender",
            "demo"
    )
    
    if not viewed_events_only:
        expanded_tuning = helpers.add_tuning_without_viewing(
            clone_dataframe(demo_data), 
            swapped_tuning
        )
    else:
        expanded_tuning = swapped_tuning
    
    # 4. FINAL NPM TUNING/VIEWING EVENTS   
    panel_data = expanded_tuning.drop(
        "age",
        "gender",
        "demo"
    ).join(
        clone_dataframe(demo_data),
        on=[
            "intab_period_id",
            "household_media_consumer_id",
            "location_media_consumer_id",
            "person_media_consumer_id"
        ],
        how="inner"
    ).withColumn(
        "genre_survey",
        collapse_genre("genre")
    ).withColumn( # Add a flag to indicate if the event has a viewer
        "viewer_flag",
        F.when(
            F.col("viewing_start_utc").isNull(),
            F.lit("N")
        ).otherwise(
            F.lit("Y")
        )
    ).withColumn( # Add a viewing id for each tuning event
        "viewing_id",
        F.md5(
            F.concat(
                F.col("media_engagement_event_key"),
                F.col("tuning_start_utc").cast("string"),
                F.col("tuning_end_utc").cast("string")
            )
        )   
    ).withColumn(
        "tuning_duration_hrs",
        calculate_duration("tuning_start_utc", "tuning_end_utc")
    ).withColumn(
        "viewing_duration_hrs",
        calculate_duration("viewing_start_utc", "viewing_end_utc")
    ).drop(
        "household_member",
        "household_members",
        "household_viewers",
        "gap",
        "type",
        "rank",
        "count",
        "swap",
        "vid",
        "n_views",
        "viewing_rank",
        "copy",
        "non_viewer"
    )
    
    return panel_data
