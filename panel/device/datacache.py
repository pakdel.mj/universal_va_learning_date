import re
import datetime
from dss_datacache_client import client


def load_device_files( # Ideally this turns into an MDL query someday
    spark, 
    query_string, 
    id_string, 
    start_date, 
    end_date
):
    """
    Helper function to load a sequence of device queries/files from Data 
    Science Studio data caches. The files must be named after the following 
    regex pattern:
    
        `-Aplnc_*-parquet`
        
    The wildcard part of this pattern should be a date string of the form
    `%b_%y` (i.e Feb_19, Mar_19, Apr_19).
    
    To avoid passing around extra parameters, we use the same `start_date` and
    `end_date` parameters defined for the tuning pulls. Device files are 
    partitioned such that we have one file per month, so we can use python
    `datetime` magic to convert to the `%b_%y` format of the device file 
    names.
    
    Args:
    -----
    query_string: `str`, required
        A string we can use to match on the `query` parameter of files
        residing in the datacache
    id_string: `str`, required
        A string we can use to match on the `run_id` parameter of files in 
        the datacache (`run_id` is usually a timestamp so usually it is 
        sufficient to pass a date string in YYYY-mm-dd format)
    start_date: `str`, required
        Date string in YYYY-mm-dd format which defines the start of the range 
        of device files we will load
    end_date: `str`, required
        Date string in YYYY-mm-dd format which defines the end of the range of
        device files we will load
        
    Returns:
    --------
    A sorted `list` where each element is a `tuple` containing the date string
    extracted from the device file and a `pyspark.sql.dataframe.DataFrame`
    containing the loaded device data. There will be one element in the list
    for each device file that falls with the specified date range.
    """
    
    start_month_year = datetime.datetime.strptime(
        start_date, "%Y-%m-%d"
    ).strftime("%Y%m")
    
    end_month_year = datetime.datetime.strptime(
        end_date, "%Y-%m-%d"
    ).strftime("%Y%m")
    
    files = client.search(
        query=query_string, 
        run_id=id_string, 
        match_type="contains"
    )
    
    dates = [datetime.datetime.strptime(
        re.search(
            "-Aplnc_(.+?)-parquet",
            f.query
        ).group(1).replace("_", " "),
        "%b %y"
    ).strftime("%Y%m") for f in files]
    
    dfs = [f.to_spark(spark) for f in files]
    
    inputs = sorted(filter(
        lambda x: (x[0] >= start_month_year) & (x[0] <= end_month_year),
        zip(dates, dfs)
    ), key=lambda x: x[0])
    
    return inputs
