from pyspark.sql import Window
import pyspark.sql.functions as F

from .conditions import get_device_code, get_internet_capable
from .datacache import load_device_files


def format_device_data(df, date):
    """
    Accepts a dataframe of device data and performs some type casting and 
    string casting steps. All fields from the device files are coming in as 
    strings, so we will do some trimming and lowercasing as well. We also 
    rename the columns to something a bit more friendly.
    
    Args:
    -----
    df: `pyspark.sql.dataframe.DataFrame`, required
        The device data to format
    date: `str` or `int`, required:
        A value we will append to the data for sorting purposes
        
    Returns:
    --------
    A pyspark dataframe containing the formatted device data with the 
    `QFMMonth` field added so that we can determine the most recent
    record for each device
    """
    
    formatted = df.withColumn(
        "DeviceMake",
        F.lower(F.trim(F.col("Make")))
    ).withColumn(
        "DeviceModel",
        F.lower(F.trim(F.col("Mdl")))
    ).fillna(
        "", ["DeviceMake", "DeviceModel"]
    ).withColumn(
        "Device",       
        F.concat(
            F.col("DeviceMake"),
            F.lit(" "),
            F.col("DeviceModel")
        )
    )
    
    renamed = formatted.select(
        F.col("InstldHsngUnitNum").alias(
            "InstalledHousingUnitNum"
        ).cast("long"),
        F.col("HsngUnitId").alias("HousingUnitId"),
        F.col("HomeUnitId"),
        F.col("SiteUnitId").cast("integer"),
        F.col("AplncId").alias("DeviceId"),
        F.col("AplncTypCd").alias("DeviceType"),
        F.col("DeviceMake"),
        F.col("DeviceModel"),
        F.col("Device"),
        F.col("IntrntCapable").alias("InternetCapable"),
        F.col("IntrntEnabled").alias("InternetEnabled"),
        F.lit(date).alias("QFMMonth")
    )
    
    return renamed.drop_duplicates()


def clean_device_data(df):
    """
    Accepts formatted device data and performs an adjustment on the 
    `InternetCapable` and calculates a `ReportingDeviceCode` using the 
    `DeviceType` and `Device` fields. We use the `QFMMonth` field to perform
    a deduplication step wherein we select the most recent device information
    for each combination of `InstalledHousingUnitNum` and `HousingUnitId`. 
    Finally, we limit the devices to only those that are internet-capable and
    of a certain `DeviceType`.
    """
    clean = df.withColumn(
        "InternetCapable",
        get_internet_capable()
    ).withColumn(
        "ReportingDeviceCode",
        get_device_code()
    ).withColumn(
        "rank",
        F.rank().over(
            Window.partitionBy(
               "InstalledHousingUnitNum", 
               "HousingUnitId" 
            ).orderBy(
                F.col("QFMMonth").desc()
            )
        )
    ).filter( # Only keep the most recent device info
        (F.col("rank")==1) &
        (F.col("InternetCapable")=="Y") &
        (F.col("InternetEnabled")=="Y") &
        (~F.col("ReportingDeviceCode").isNull()) &
        (F.col("DeviceType").isin([
            "1", "5", "6", "8", "11"
         ])
        ) & # TV, DVDR, GAME, DVD, Internet
        (~F.col("DeviceModel").isin([
            "portable media player",
            "smart phones",
            "play station 1",
            "nintendo 64"
         ])
        ) 
    ).drop(
        "QFMMonth",
        "rank"
    )
     
    return clean


def make_exclusion(df):
    """
    Accepts the cleaned device data and creates a table of devices 
    that should be excluded from further analysis.
    """
    
    primary_keys = [
        "InstalledHousingUnitNum",
        "HousingUnitId",
        "HomeUnitId",
        "SiteUnitId",
        "Device"
    ]
    
    tv = df.filter(
        F.col("DeviceType")=="1"
    ).select(
        primary_keys
    ).distinct()
    
    internet_sub = df.filter(
        (F.col("DeviceType")=="11") &
        (~F.col("ReportingDeviceCode").isin(["RKU", "GGL"]))
    )
    
    subset1 = tv.join(
        internet_sub,
        on=primary_keys,
        how="inner"
    )
    
    consoles = df.filter(
        F.col("DeviceType").isin(["6", "8"])
    ).select(
        primary_keys
    ).distinct()
    
    internet_con = df.filter(
        F.col("DeviceType")=="11"
    )
    
    subset2 = consoles.join(
        internet_con,
        on=primary_keys,
        how="inner"
    )
    
    subset3 = df.filter(
        (F.col("DeviceType")=="11") &
        (
            F.col("DeviceModel").like("smart tv%") |
            F.col("DeviceModel").like("%hdtv%")
        )
    )

    return subset3.unionByName(
        subset1
    ).unionByName(
        subset2
    ).distinct()


def aggregate_device_data(df):
    """
    Accepts the final device data set and creates a pivot table where each row
    is a household unit and each column contains counts of each respective 
    `ReportingDeviceCode`. We also add a column to quantify the total number
    of OTT-enabled devices in the home.
    """
    
    base = df.select( # Distinct devices
        "InstalledHousingUnitNum",
        "SiteUnitId",
        "ReportingDeviceCode",
        F.lit(1).alias("DeviceCount") # Counter column
    ).distinct()
    
    agg = base.groupby( # Household-level pivot table
        "InstalledHousingUnitNum",
        "SiteUnitId"
    ).pivot(
        "ReportingDeviceCode"
    ).agg(
        F.sum(
            F.col("DeviceCount")
        )
    ).fillna(0)
    
    # This column is never referenced later
#     ott = base.groupby( # Calculate OTT-Enabled
#         "InstalledHousingUnitNum",
#         "SiteUnitId"
#     ).agg(
#         F.sum(
#             F.col("DeviceCount")
#         ).alias("OTT")
#     )
    
    return agg#.join( # Merge and return
#         ott,
#         on=[
#             "InstalledHousingUnitNum",
#             "SiteUnitId"
#         ],
#         how="inner"
#     )


def device_pull(
    spark, 
    query_string, 
    id_string,
    start_date,
    end_date
):
    """
    Searches for files in the datacache, converts any matching files to 
    individual Spark dataframes and performs a union by column name on all of 
    them. Pretty sure this function will only work with parquet files. It is 
    also imperative that all matching files returned from the search must 
    contain the same columns.
    
    We also perform the formatting and cleaning steps before returning a
    cached dataframe. We want to cache this because we will be referring to it
    often when creating the exclusion table and performing the final device
    aggregation steps.
    
    Args:
    -----
    spark: `pyspark.sql.session.SparkSession`, required
        A Pypsark session we can use to convert the parquet files to 
        `pyspark.sql.dataframe.DataFrame` objects
    query_string: `str`, required
        A string we can use to match on the `query` parameter of files
        residing in the datacache
    id_string: `str`, required
        A string we can use to match on the `run_id` parameter of files in 
        the datacache (`run_id` is usually a timestamp so usually it is 
        sufficient to pass a date string in YYYY-mm-dd format)
        
    Returns:
    --------
    A pyspark dataframe containing the device data
    """
    
    inputs = load_device_files(
        spark,
        query_string, 
        id_string, 
        start_date, 
        end_date
    )
    
    data = format_device_data(inputs[0][1], inputs[0][0])
    
    for date, df in inputs[1:]:        
        data = data.unionByName(
            format_device_data(df, date)
        )
        
    return clean_device_data(data).cache()