import pyspark.sql.functions as F


# Constant - Resets internet capability on certain devices
def get_internet_capable():
    return F.when(
        F.col("InternetCapable")=="N",
        F.when(
            (F.col("DeviceType")=="6") &
            (
                F.col("DeviceModel").like("play%station%3") |
                F.col("DeviceModel").like("play%station%4") |
                F.col("DeviceModel").like("%wii%") |
                F.col("DeviceModel").like("%switch%") |
                F.col("DeviceModel").like("x%box%")
            ),
            F.lit("Y")

        ).when(
            (F.col("DeviceType")=="11") &
            (
                F.col("DeviceModel").like("google chromecast") |
                F.col("DeviceModel").like("apple tv") |
                F.col("DeviceModel").like("amazon fire%") |
                F.col("DeviceModel").like("neo tv") |
                F.col("DeviceModel").like("%roku%")
            ),
            F.lit("Y")
        ).otherwise(
            F.col("InternetCapable")
        )
    ).otherwise(
        F.col("InternetCapable")
    )


# Constant - when clauses to create device reporting codes
def get_device_code():
    return F.when(
        F.col("DeviceType")=="11",
        F.when(
            F.col("Device").like("%apple%"),
            F.lit("APL")
        ).when(
            F.col("Device").like("%roku%") |
            F.col("Device").like("%ruku%") |
            F.col("Device").like("%r0ku%") |
            F.col("Device").like("insignia ns 32dr310na17") | # Roku Hybrid
            F.col("Device").like("tcl 28s3750"), # Roku Hybrid
            F.lit("RKU")
        ).when(
            F.col("Device").like("%google%") |
            F.col("Device").like("vizio%%smart%cast%") | # Chromecast Hybrid
            F.col("Device").like("%chrom%"),
            F.lit("GGL")
        ).when(
            F.col("Device").like("%amazon%"),
            F.lit("AMN")
        ).otherwise(
            F.lit("STB")
        )
    ).otherwise(
        F.when(
            F.col("DeviceType")=="6",
            F.when(
                F.col("Device").like("%play%station%"),
                F.lit("PSX")
            ).when(
                F.col("Device").like("%x%box%"),
                F.lit("XBX")
            ).otherwise(
                F.lit("STB")
            )
        ).otherwise(
            F.when(
                F.col("DeviceType").isin(["8", "5"]),
                F.lit("DVD")
            ).when(
                F.col("DeviceType")=="1",
                F.lit("STV")
            ).otherwise(
                F.lit("STB")
            )
        )
    )
