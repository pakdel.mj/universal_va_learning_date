import pyspark.sql.functions as F
from ..utils import array_intersect
    
    
def get_vampire_codes():
    return F.array(# This is how we are indentifying vampires
        F.lit(1),  # Local Market TV Diary Only
        F.lit(2),  # Local Market TV Set Meter
        F.lit(4),  # Local Market Hispanic TV Diary Only
        F.lit(5),  # Local Market Hispanic TV Set Meter
        F.lit(9),  # Local Market TV Diary - Meter Adjusted
        F.lit(10), # Local Market Supplemental TV Diary in LPM Market
        F.lit(11), # Local Market Pre-Production TV Set Meter
        F.lit(13), # Local Market Supplemental Hispanic TV Diary in LPM Market
        F.lit(14), # Local Market Hispanic Pre-Production TV Set Meter
        F.lit(16), # Local Market Hispanic TV Diary - Meter Adjusted
        F.lit(17), # Local Market Mailable TV Meter
        F.lit(18), # Local Market Mailable TV Meter
        F.lit(19), # Hotelevision TV Set Meter
        F.lit(30), # National RDD Home Panel
        F.lit(31), # National RDD Work Panel
        F.lit(32), # National RDD Home+Work Panel
        F.lit(83)  # No description, but it does exist
    )


def get_tvpc_codes():
    return F.array(
        F.lit(20), # Convergence Panel (TV&PC)
        F.lit(21), # Convergence Panel Plus (TV&PC)
        F.lit(22), # I2 NHTI PS (TV Side)
        F.lit(23), # I2 NHTI NPS (TV Side)
        F.lit(24), # I2 NPM/LPM FTO
        F.lit(25), # I2 NSI Redtrofit
        F.lit(26), # I2 Sim Mand/All PCs
        F.lit(27), # I2 Sim Mand/Prim PC+
        F.lit(28), # I2 Sim Opt All PCs
        F.lit(29)  # I2 Sim Opt Prim PC+
    )


def flag_tvpc(collection_methods):
    return F.when(
        F.size(
            array_intersect(
                F.col(collection_methods),
                get_tvpc_codes()
            )
        )>0,
        F.lit(True)
    ).otherwise(
        F.lit(False)
    )


def make_household_type(collection_methods):
    return F.when(
        F.size(
            array_intersect(
                F.col(collection_methods),
                get_vampire_codes()
            )
        )>0,
        F.lit("vampire")
    ).when(
        F.size(
            array_intersect(
                F.col(collection_methods),
                F.array(F.lit(3), F.lit(7))
            )
        )==2,
        F.lit("hybrid")
    ).when(
        F.array_contains(
            F.col(collection_methods),
            3
        ),
        F.lit("lpm")
    ).when(
        F.array_contains(
            F.col(collection_methods),
            7
        ),
        F.lit("npm")
    ).otherwise(
        F.lit("other")
    )


def filter_collection_method(df, collection_method):
    if collection_method == "hybrid":
        return df.filter(
            F.col("household_type").isin(["hybrid","npm"])
        )
    elif collection_method == "npm":
        return df.filter(
            F.col("household_type")=="npm"
        )
    elif collection_method == "tvpc":
        return df.filter(
            (F.col("household_type")!="vampire") &
            (F.col("is_tvpc"))
        )
    else:
        return df
    
    
def make_demo_bucket(age):
    return F.when( 
        F.col(age).between(2,12), 
        F.lit("2-12")
    ).when(
        F.col(age).between(13,14),
        F.lit("13-14")
    ).when(
        F.col(age).between(15,17),
        F.lit("15-17")
    ).when(
        F.col(age).between(18,20),
        F.lit("18-20")
    ).when(
        F.col(age).between(21,24),
        F.lit("21-24")
    ).when(
        F.col(age).between(25,29),
        F.lit("25-29")
    ).when(
        F.col(age).between(30,34),
        F.lit("30-34")
    ).when(
        F.col(age).between(35,39),
        F.lit("35-39")
    ).when(
        F.col(age).between(40,44),
        F.lit("40-44")
    ).when(
        F.col(age).between(45, 49),
        F.lit("45-49")
    ).when(
        F.col(age).between(50,54),
        F.lit("50-54")
    ).when(
        F.col(age).between(55,64),
        F.lit("55-64")
    ).when(
        F.col(age) > 64,
        F.lit("65+")
    ).otherwise(
        F.lit("under 2")
    )
