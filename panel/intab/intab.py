from .household import household_pull


def get_calendar_dates(spark, start_date, end_date):
    """
    Fetch the appropriate intab dates for a given date range. Keeping this 
    table cached allows us to quickly trim the fat out of all of our other
    data merges.
    
    Args:
    -----
    spark: `pyspark.sql.session.SparkSession`, required
        A Pyspark session we can use to query the MDL
    start_date: `str`, required
        Date string in YYYY-mm-dd format defining the start of the range
    end_date: `str`, required
        Date string in YYYY-mm-dd format defining the end of the range
        
    Returns:
    --------
    A `pyspark.sql.dataframe.DataFrame` containing the selected intab dates
    """
    
    calendar = spark.sql(
        "SELECT calendar_date_id as intab_period_id, "
               "calendar_date, "
               "calendar_day_of_week_number as week_number "
        "FROM tam_npm_mch_tv_prod.calendar_date "
        "WHERE calendar_date between '{start}' AND '{end}'".format(
            start=start_date, 
            end=end_date
        )
    )
    
    return calendar


def get_person_intab(spark, start_date, end_date):
    """
    Get all of the intab panelists who fall within a predefined date range.
    """
    
    calendar = get_calendar_dates(
        spark, start_date, end_date
    )
    
    intab = spark.sql("""
        SELECT intab_period_id,
               household_media_consumer_id,
               location_media_consumer_id,
               person_media_consumer_id
        FROM tam_npm_mch_tv_prod.metered_tv_person_intab_period
        WHERE released_for_processing_flag = 'Y'
    """)
    
    subset = intab.join(
        calendar, 
        on="intab_period_id", 
        how="inner"
    )
    
    return subset


def get_household_intab(
    spark, 
    start_date, 
    end_date, 
    collection_method="hybrid"
):
    """
    Get all of the intab households who fall within a predefined date range.
    """
    
    calendar = get_calendar_dates(spark, start_date, end_date)
    
    intab = spark.sql("""
        SELECT intab_period_id, 
               location_media_consumer_key,
               household_media_consumer_id,
               location_media_consumer_id
        FROM tam_npm_mch_tv_prod.metered_tv_household_location_intab_period
        WHERE released_for_processing_flag = 'Y'
    """)
    
    subset = intab.join(
        calendar, 
        on="intab_period_id", 
        how="inner"
    )
    
    in_scope = household_pull(spark, subset, collection_method)
    return in_scope


def get_vod_intab(spark, start_date, end_date):
    """
    Get all of the VOD panelists who are intab within a predefined range.
    """

    calendar = get_calendar_dates(spark, start_date, end_date)
    
    intab = spark.sql("""
        SELECT intab_period_id,
               household_media_consumer_id,
               location_media_consumer_id,
               vod_service_provider_list_key
        FROM tam_npm_mch_tv_prod.metered_tv_household_location_vod_service_provider_intab_period
        WHERE released_for_processing_flag = 'Y'
    """)
    
    subset = intab.join(
        calendar,
        on="intab_period_id",
        how="inner"
    )
    
    return subset
