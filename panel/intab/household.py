from pyspark.sql import Window
import pyspark.sql.functions as F

from .conditions import make_household_type, flag_tvpc, filter_collection_method
from ..utils import clone_dataframe


def get_dma_ids(spark):
    
    dmas = spark.sql("""
        SELECT market_id,
               designated_market_area_id,
               effective_start_date,
               effective_end_date
        FROM tam_npm_mch_tv_prod.designated_market_area
    """).groupby(
        "market_id",
        "designated_market_area_id"
    ).agg(
        F.min("effective_start_date").alias("effective_start_date"),
        F.max("effective_end_date").alias("effective_end_date")
    ).select(
        "market_id",
        "designated_market_area_id"
    )
    
    return dmas


def add_household_features(df):
    """
    Accepts a dataframe of household count data and converts to features that
    will be used in the VAM model. The raw count fields are not used 
    downstream so they are dropped.
    """
    
    model = df.withColumn(
        "hh_size_model",
        F.when(
            F.col("hh_size") < 5,
            F.col("hh_size").cast("string")
        ).otherwise(
            F.lit("5+")
        )
    ).withColumn(
        "adults_model",
        F.when(
            F.col("n_adults") < 3,
            F.lit("<=2")
        ).otherwise(
            F.lit("3+")
        )
    ).withColumn(
        "kids_model",
        F.when(
            F.col("n_kids") < 3,
            F.col("n_kids").cast("string")
        ).otherwise(
            F.lit("3+")
        )
    ).withColumn(
        "kids_presence",
        F.when(
            F.col("n_kids") > 0,
            F.lit("1")
        ).otherwise(
            F.lit("0")
        )
    )
    
    return model
    

def aggregate_household_counts(df):
    """
    Accepts a dataframe of tuning data and aggregates to calculate household
    size, the number of adults and the number of kids for each household.
    """
    
    size = df.groupby( # Household size
        "intab_period_id",
        "household_media_consumer_id",
        "location_media_consumer_id"
    ).agg(
        F.countDistinct(
            F.col("person_media_consumer_id")
        ).alias("hh_size")
    ).filter(
        F.col("hh_size") > 1 # Only households > 1 for VAM
    )
    
    adults = df.filter( # If we use a pivot we can do kids/adults together
        F.col("age")>=18
    ).groupby(
        "intab_period_id",
        "household_media_consumer_id",
        "location_media_consumer_id"
    ).agg(
        F.countDistinct(
            F.col("person_media_consumer_id")
        ).alias("n_adults")
    )
    
    kids = df.filter(
        F.col("age")<18
    ).groupby(
        "intab_period_id",
        "household_media_consumer_id",
        "location_media_consumer_id"
    ).agg(
        F.countDistinct(
            F.col("person_media_consumer_id")
        ).alias("n_kids")
    )

    features = size.join(
        clone_dataframe(adults),
        on=[
            "intab_period_id",
            "household_media_consumer_id",
            "location_media_consumer_id"
        ],
        how="left"
    ).join(
        clone_dataframe(kids),
        on=[
            "intab_period_id",
            "household_media_consumer_id",
            "location_media_consumer_id"
        ],
        how="left"
    ).fillna(
        0, ["n_adults", "n_kids"]
    )
    
    return features

        
def household_pull(spark, household_intab, collection_method="hybrid"):
    """
    Pulls household information from the `tam_npm_mch_tv_prod.metered_tv_
    household_location_sample_collection_method`. We only accept households
    where the `sample_collection_method_key` is equal to 7. An additional
    criterion is applied whereby the household can only be sampled by one
    and only one `sample_collection_method_key` during each intab period.
    
    Args:
    -----
    spark_session: `pyspark.sql.session.SparkSession`, required
        A Pyspark session we can use to connect to the MDL
    start_date: `str`, required
        Date string in YYYY-mm-dd format defining the start of the range
    end_date: `str`, required
        Date string in YYYY-mm-dd format defining the end of the range
    collection_method: `string`, optional
        If set to `hybrid` we will include all tuning events originating from 
        people meters; if `npm` we will include only tuning events from NPM
        households; if `lpm` we will only include events from LPM houses; if 
        `tvpc` we will only include events from the TV/PC panel; and if `all`
        we will return all intab panelists (including vampires)
                
    Returns:
    --------
        A `pyspark.sql.dataframe.DataFrame` containing the household and 
        intab IDs that met the selection criteria
    """
    
    if collection_method not in (
        "hybrid",
        "npm",
        "tvpc",
        "all"
    ):
        raise ValueError(
            "Unrecognized collection_method. Acceptable values are 'hybrid', "
            "'npm', 'lpm' or 'tvpc'"
        )
     
    data = spark.sql("""
        SELECT intab_period_id,
               household_media_consumer_id,
               location_media_consumer_id,
               sample_collection_method_key
        FROM tam_npm_mch_tv_prod.metered_tv_household_location_sample_collection_method
        WHERE released_for_processing_flag = 'Y'
    """).join(
        household_intab,
        on=[
            "intab_period_id",
            "household_media_consumer_id",
            "location_media_consumer_id"
        ],
        how="inner"
    ).groupby(
        "intab_period_id",
        "location_media_consumer_key",
        "household_media_consumer_id",
        "location_media_consumer_id",
    ).agg(
        F.collect_set(
            F.col("sample_collection_method_key")
        ).alias("collection_methods"),
        F.first("week_number").alias("week_number")
    ).select(
        "intab_period_id",
        "location_media_consumer_key",
        "household_media_consumer_id",
        "location_media_consumer_id",
        make_household_type(
            "collection_methods"
        ).alias("household_type"),
        flag_tvpc("collection_methods").alias("is_tvpc"),
        "week_number"
    )
    
    return filter_collection_method(data, collection_method)
