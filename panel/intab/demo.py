from pyspark.sql import Window
import pyspark.sql.functions as F

from .conditions import make_demo_bucket


def demo_pull(spark, end_date):
    """
    Pulls demo information from the `tam_npm_mch_tv_prod.metered_tv_person` 
    table on the MDL. This function will automatically covert the gender 
    code to M or F and will also calculate panelist ages based on the 
    supplied `end_date`
    
    Args:
    -----
    spark: `pyspark.sql.session.SparkSession`, required
        A Spark session we can use to access and query the MDL
    end_date: `str`, required
        Date string in YYYY-mm-dd which we use along with panelist birth
        dates to calculate ages.
    
    Returns:
    --------
    A `pyspark.sql.dataframe.DataFrame` containing the raw demo info
    """

    demos = spark.sql("""
        SELECT household_media_consumer_id,
               source_installed_household_number,
               person_media_consumer_id,
               source_person_number,
               date_of_birth,
               gender_code,
               creation_datetime
        FROM tam_npm_mch_tv_prod.metered_tv_person
        WHERE is_short_term_visitor_flag = 'N'
    """).withColumn( # Determine most recent entry for each panelist
        "row_number",
        F.row_number().over(
            Window.partitionBy(
                "household_media_consumer_id",
                "source_installed_household_number",
                "person_media_consumer_id",
                "source_person_number"
            ).orderBy(
                F.col("creation_datetime").desc()
            )
        )
    ).filter( # Only keep the most recent entry
        F.col("row_number") == 1
    ).withColumn( # Convert gender codes
        "gender",
        F.when(
            F.col("gender_code")==0,
            F.lit("F")
        ).otherwise( # Can we just assume M here?
            F.when(
                F.col("gender_code")==1,
                F.lit("M")
            )
        )
    ).withColumn( # Calculate panelist ages 
        "age",
        F.floor( 
            F.months_between(
                F.lit(end_date), 
                F.col("date_of_birth")
            ) / 12
        )
    )
    
    return demos


def make_demos(df):
    """
    Accepts the pulled demo data as input and applies the bucketing scheme on
    the `age` column. The final `demo` column will be the gender concatenated 
    with the age bucket. We also remove any panelists with ages less than 2 
    (we do observe panelists with negative ages in this data).
    
    Args:
        df: `pyspark.sql.dataframe.DataFrame`, required
            The output of the `pull_data` function
    Returns:
        A `pypsark.sql.dataframe.DataFrame` containing all of demo info we 
        need for VAM
    """
    
    buckets = df.withColumn(
        "demo",
        make_demo_bucket("age") 
    ).filter(
        F.col("demo")!="under 2"
    ).withColumn(
        "demo",
        F.concat(
            F.col("gender"),
            F.col("demo")
        )
    ).select(
        "household_media_consumer_id",
        "person_media_consumer_id",
        "age",
        "gender",
        "demo"
    )
    
    return buckets
