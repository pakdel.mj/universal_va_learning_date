def add_collapsed_genre(spark, df):
    """
    Calculate event durations and dayparts on a spark dataframe
    
    Args:
    -----
    df: `pyspark.sql.dataframe.DataFrame`, required
        A data frame of tuning events; the output of the `pull_data` function
    
    Returns:
    --------
    A `pyspark.sql.dataframe.DataFrame` containing formatted tuning events
    """
    a = 1

    f = "s3://us-east-1-nlsn-w-tam-ltamingestion-gtam-prod-01/data/panel_ingestion/external/AUDIENCE_IMPUTATION_TV_MEDIA_CREDIT_GENRE/effectivestartdate=20200401"
    names = [
        "METERED_TV_CREDIT_STATION_CODE",
        "TV_MEDIA_CREDIT_DESC",
        "TV_MEDIA_CREDIT_SHORT_DESC",
        "INTERNAL_CALL_LETTERS",
        "DISTRIBUTOR_TYPE_CODE",
        "MARKET_OF_ORIGIN_ID",
        "NATIONAL_SERVICE_LEGACY_SERVICE_CODE",
        "GENRE_TYPE_CODE",
        "GENRE_TYPE_DESC",
        "GENRE_ALPHA_CODE",
        "GENRE_DESC",
        "CAPPING_GENRE_ID",
        "CAPPING_GENRE_DESC",
        "LOCATION_TYPE",
        "LOCATION_CODE",
        "EFFECTIVE_START_DATE",
        "EFFECTIVE_END_DATE",
        "RELEASED_FOR_PROCESSING_FLAG",
        "PRIORITY_INDICATOR",
        "AI_STATION_IND",
        "UNIQUE_ID",
        "LOAD_ID",
        "CREATION_DATETIME",
        "LAST_UPDATED_DATETIME",
        "LAST_UPDATED_BY_LOAD_ID",
    ]
    station_genre_mapping = (
        spark.read.option("delimiter", ",").option("inferSchema", "true").csv(f)
    )

    for i, name in enumerate(names):
        station_genre_mapping = station_genre_mapping.withColumnRenamed(
            "_c" + str(i), name
        )

    station_genre_mapping = station_genre_mapping.filter("LOCATION_CODE = 11")

    df = df.join(
        station_genre_mapping, on=["METERED_TV_CREDIT_STATION_CODE"], how="left"
    )

    return df
