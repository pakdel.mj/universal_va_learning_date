from pyspark.sql import Window
import pyspark.sql.functions as F

from ..intab import intab


def provider_pull(spark, start_date, end_date):
    """
    Pulls intab information for a given service provider from the MDL. We use 
    the `tam_npm_mch_tv_prod.metered_tv_household_location_vod_service_
    provider` table to determine which VOD service is being used in each
    household. We also use a join to subset to only intab panelists.
    
    Args:
    -----
    spark: `pyspark.sql.session.SparkSession`, required
        A Pyspark session we can use to query the MDL
    start_date: `str`, required
        Date string in YYYY-mm-dd format defining the start of the range
    end_date: `str`, required
        Date string in YYYY-mm-dd format defining the end of the range
        
    Returns:
    --------
    A `pyspark.sql.dataframe.DataFrame` containing the household and intab 
    IDs for the given provider
    """

    intab_df = intab.get_vod_intab(spark, start_date, end_date)
    
    vod = spark.sql("""
        SELECT vod_service_provider_list_key, 
               vod_service_provider_id 
        FROM tam_npm_mch_tv_prod.metered_tv_household_location_vod_service_provider 
    """)
    
    service = spark.sql("""
        SELECT vod_service_provider_id,
               vod_service_provider_name as provider
        FROM tam_npm_mch_tv_prod.vod_service_provider
    """)
    
    data = vod.join(
        intab_df,
        on="vod_service_provider_list_key",
        how="inner"
    ).join(
        service,
        on="vod_service_provider_id",
        how="inner"
    ).groupby(
       "intab_period_id",
        "household_media_consumer_id",
        "location_media_consumer_id",
    ).agg(
        F.collect_set("provider").alias("vod_providers")
    )
        
    return data
