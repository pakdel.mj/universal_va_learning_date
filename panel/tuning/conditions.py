import pyspark.sql.functions as F
from ..utils import array_intersect
    
    
def calculate_daypart(weekday, hour): 
    return F.when( # All days
        (F.col(hour) < 6) | (F.col(hour) > 22),
        F.lit("Late Fringe")
    ).otherwise(
        F.when( # Weekdays
            F.col(weekday).between(1,5),
            F.when(
                F.col(hour).between(6,9),
                F.lit("Weekday Morning")
            ).when(
                F.col(hour).between(10,17),
                F.lit("Weekday Daytime")
            ).when(
                F.col(hour).between(18,19),
                F.lit("Early Fringe")
            ).when(
                F.col(hour).between(20, 22),
                F.lit("Prime Time")
            ).otherwise(
                F.lit("UNDEFINED DAYPART")
            )
        ).otherwise(
            F.when( # Saturday
                F.col(weekday)==6,
                F.when(
                    F.col(hour).between(6,17),
                    F.lit("Weekend Daytime")
                ).when(
                    F.col(hour).between(18,19),
                    F.lit("Early Fringe")
                ).when(
                    F.col(hour).between(20, 22),
                    F.lit("Prime Time")
                ).otherwise(
                    F.lit("UNDEFINED DAYPART")
                )
            ).otherwise(
                F.when( # Sunday
                    F.col(weekday)==7,
                    F.when(
                        F.col(hour).between(6,17),
                        F.lit("Weekend Daytime")
                    ).when(
                        F.col(hour)==18,
                        F.lit("Early Fringe")
                    ).when(
                        F.col(hour).between(19,22),
                        F.lit("Prime Time")
                    ).otherwise(
                        F.lit("UNDEFINED DAYPART")
                    )
                )
            )
        )
    )
