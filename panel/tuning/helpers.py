import datetime

from pyspark.sql import Window
import pyspark.sql.functions as F
import pyspark.sql.types as T

from ..utils import clone_dataframe
from ..utils import array_distinct, subtract_timestamps


@F.udf(T.ArrayType(T.ArrayType(
    T.StructType([
        T.StructField("credit", T.TimestampType()),
        T.StructField("local", T.TimestampType()),
        T.StructField("utc", T.TimestampType())
    ])
)))
def make_continuous_sequence(start_times, end_times):
    if len(start_times) == len(end_times) == 1:
        return None

    dates = sorted(start_times + end_times, key=lambda x: x["utc"])
    one_second = datetime.timedelta(seconds=1)
    
    intervals = [[dates[0]]]
    end_time = dates[-1]
    
    for d in dates[1:]:
        p = intervals[-1][-1]
        if p["utc"] == end_time["utc"]:
            break

        this_type = "start" if d["utc"].second == 0 else "end"
        prev_type = "start" if p["utc"].second == 0 else "end"

        if this_type == "start":
            if prev_type == "end":
                intervals.append([d])
            else:
                if d["utc"] != p["utc"]:
                    n = {}
                    for t in ("utc", "credit", "local"):
                        n[t] = d[t] - one_second
                    intervals[-1].append(n)
                    intervals.append([d])
        
        if this_type == "end":
            if prev_type == "start":
                intervals[-1].append(d)
                if d["utc"] != end_time["utc"]:
                    n = {}
                    for t in ("utc", "credit", "local"):
                        n[t] = d[t] + one_second
                    intervals.append([n])
            else:
                intervals.append([d])
                if d["utc"] != end_time["utc"]:
                    n = {}
                    for t in ("utc", "credit", "local"):
                        n[t] = d[t] + one_second
                    intervals[-1].append(d) 
    
    for ((os, oe), (ns, ne)) in zip(
        zip(start_times, end_times), 
        intervals
    ):
        if os["utc"] != ns["utc"] or oe["utc"] != ne["utc"]:
            return intervals
        
    return None


def make_tuning_times_consistent(adjusted_df):
    
    start_cols = [
        ("tuning_credit_start_utc", "credit"),
        ("tuning_start_local", "local"),
        ("tuning_start_utc", "utc")
    ]
    
    end_cols = [
        ("tuning_credit_end_utc", "credit"),
        ("tuning_end_local", "local"),
        ("tuning_end_utc", "utc")
    ]
    
    tuning_times = adjusted_df.groupby(
        "media_engagement_event_key"
    ).agg(
        F.collect_set(
            F.struct(*[F.col(a).alias(b) for a, b in start_cols])
        ).alias("tuning_starts"),
        F.collect_set(
            F.struct(*[F.col(a).alias(b) for a, b in end_cols])
        ).alias("tuning_ends")
    ).select(
        "media_engagement_event_key",
        make_continuous_sequence(
            F.col("tuning_starts"),
            F.col("tuning_ends")
        ).alias("new_tuning")
    )
    
    consistent = adjusted_df.join(
        clone_dataframe(tuning_times), 
        on="media_engagement_event_key", 
        how="inner"
    ).withColumn(
        "new_tuning",
        F.explode_outer(F.col("new_tuning"))
    )
    
    for col, alias in start_cols:
        consistent = consistent.withColumn(
            col,
            F.when(
                F.col("new_tuning").getItem(0)[alias].isNull(),
                F.col(col)
            ).otherwise(
                F.col("new_tuning").getItem(0)[alias]
            )
        )
        
    for col, alias in end_cols:
        consistent = consistent.withColumn(
            col,
            F.when(
                F.col("new_tuning").getItem(1)[alias].isNull(),
                F.col(col)
            ).otherwise(
                F.col("new_tuning").getItem(1)[alias]
            )
        )
        
    good_records = consistent.filter(
        (
            (F.col("tuning_start_utc")<=F.col("viewing_end_utc"))&
            (F.col("tuning_end_utc")>=F.col("viewing_start_utc"))
        ) | ( # make sure we keep tuning without viewing
            F.col("viewing_start_utc").isNull()
        )
    ).drop(
        "swap",
        "n_views",
        "new_tuning"
    )
    
    return good_records


def add_tuning_without_viewing(demo_df, consistent_df):
    
    household_members = demo_df.groupby(
        "intab_period_id",
        "household_media_consumer_id"
    ).agg(
        F.collect_set(
            F.col("person_media_consumer_id")
        ).alias("household_members")
    )
    
    # Get all household members who tuned to an event
    household_exposure = consistent_df.groupby(
        "intab_period_id",
        "media_engagement_event_key",
        "household_media_consumer_id",
        "tuning_start_utc",
        "tuning_end_utc",
    ).agg(
        F.collect_set(
            F.col("person_media_consumer_id")
        ).alias("household_viewers")
    )        
    
    # Find all non-viewers for each tuning event
    flagged_tuning = household_exposure.join(
        clone_dataframe(household_members),
        on=[
            "intab_period_id",
            "household_media_consumer_id",
        ],
        how="inner"
    ).withColumn(
        "non_viewers",
        array_distinct(
            F.col("household_members"),
            F.col("household_viewers")
        )
    ).select(
        "intab_period_id",
        "media_engagement_event_key",
        "household_media_consumer_id",
        "tuning_start_utc",
        "tuning_end_utc",
        F.explode("non_viewers").alias("non_viewer")
    )
    
    copied_tuning = consistent_df.withColumn(
        "viewing_rank",
        F.row_number().over(
            Window.partitionBy(
                "media_engagement_event_key",
                "tuning_start_utc",
                "tuning_end_utc"
            ).orderBy(
                "person_media_consumer_id"
            )
        )
    ).withColumn(
        "copy",
        F.explode_outer(
            F.when(
                F.col("viewing_rank")==1,
                F.array(F.lit("original"), F.lit("copy"))
            ).otherwise(
                F.lit(None)
            )
        )
    )
    
    twv = copied_tuning.join(
        clone_dataframe(flagged_tuning.withColumn(
            "copy",
            F.lit("copy")
        )),
        on=[
            "intab_period_id",
            "media_engagement_event_key",
            "household_media_consumer_id",
            "tuning_start_utc",
            "tuning_end_utc",
            "copy"
        ],
        how="left"
    ).withColumn(
        "person_media_consumer_id",
        F.when(
            F.col("non_viewer").isNull(),
            F.col("person_media_consumer_id")
        ).otherwise(
            F.col("non_viewer")
        )
    ).filter( # Remove any copies we don't need
        F.when( # This happens when everyone in the home is watching
            (F.col("copy")=="copy")&F.col("non_viewer").isNull(),
            F.lit(False)
        ).otherwise(
            F.lit(True)
        )
    )         
    
    for col in [
        col for col in twv.columns
        if col.startswith("viewing")
    ]:
        twv = twv.withColumn(
            col,
            F.when(
                F.col("copy")=="copy",
                F.lit(None)
            ).otherwise(
                F.col(col)
            )
        )
        
    return twv
        
    
def reconcile_timestamps(tuning_with_viewing):
        
    typed = tuning_with_viewing.withColumn(
        "swap",
        F.when( # The tuning event completely encompasses the viewing event
            (F.col("viewing_start_utc")>=F.col("tuning_start_utc")) &
            (F.col("viewing_end_utc")<=F.col("tuning_end_utc")),
            F.lit("inside")
        ).otherwise(
            F.when(# The viewing event completely encompasses the tuning event
                (F.col("viewing_start_utc")<F.col("tuning_start_utc")) &
                (F.col("viewing_end_utc")>F.col("tuning_end_utc")),
                F.lit("outside")
            ).otherwise(
                F.when( # The viewing event starts before the tuning event 
                    (F.col("viewing_start_utc")<F.col("tuning_start_utc")) &
                    (F.col("viewing_end_utc")<=F.col("tuning_end_utc")),
                    F.lit("left")
                ).otherwise(
                    F.when( # The viewing event ends after the tuning event
                        (F.col("viewing_start_utc")>=F.col("tuning_start_utc")) &
                        (F.col("viewing_end_utc")>F.col("tuning_end_utc")),
                        F.lit("right")
                    ).otherwise(
                        F.lit(None)
                    )
                )
            )
        )
    )
    
    return typed


def swap_timestamps(typed_df):
    
    id_counts = typed_df.groupby(
        "media_engagement_event_key",
        "person_media_consumer_id",
        "tuning_start_utc",
        "tuning_end_utc"
    ).agg(
        F.countDistinct(
            F.col("vid")
        ).alias("n_views")
    )

    swap_df = typed_df.join(
        clone_dataframe(id_counts),
        on=[
            "media_engagement_event_key",
            "person_media_consumer_id",
            "tuning_start_utc",
            "tuning_end_utc"
        ],
        how="inner"
    )
    
    for a, b in [
        ("start_utc", "end_utc"),
        ("start_local", "end_local"),
        ("credit_start_utc", "credit_end_utc")
    ]:
    
        swap_df = swap_df.withColumn(
            "v_{}".format(a),
            F.when(
                F.col("swap").isin(["left", "outside"]),
                F.col("tuning_{}".format(a))
            ).otherwise(
                F.col("viewing_{}".format(a))
            )
        ).withColumn(
            "v_{}".format(b),
            F.when(
                F.col("swap").isin(["right", "outside"]),
                F.col("tuning_{}".format(b))
            ).otherwise(
                F.col("viewing_{}".format(b))
            )
        ).withColumn(
            "tuning_{}".format(a),
            F.when(
                F.col("swap").isin(["right", "inside"])&
                (F.col("n_views") > 1),
                F.col("viewing_{}".format(a))
            ).otherwise(
                F.col("tuning_{}".format(a))
            )
        ).withColumn(
            "tuning_{}".format(b),
            F.when(
                F.col("swap").isin(["left", "inside"])& 
                (F.col("n_views") > 1),
                F.col("viewing_{}".format(b))
            ).otherwise(
                F.col("tuning_{}".format(b))
            )
        ).withColumn(
            "viewing_{}".format(a),
            F.col("v_{}".format(a))
        ).withColumn(
            "viewing_{}".format(b),
            F.col("v_{}".format(b))
        )
    
    return swap_df.select([
        col for col in swap_df.columns
        if not col.startswith("v_")
    ])
