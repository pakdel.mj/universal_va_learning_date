import datetime
import pandas as pd
import numpy as np

import pyspark.sql.types as T
import pyspark.sql.functions as F

from pyspark.sql import Window

from ..utils import time_shift, cast_timestamp, clone_dataframe
from ..utils import subtract_timestamps, timestamp_delta

from .conditions import calculate_daypart


def fill_gaps(tuning_events_df):
    """
    Function to add "dummy" tuning events to our panel data so that we do not 
    lose any viewing minutes when we perform a range join to bring in the 
    person-level viewing.
    
    We start by partitioning the data on `media_engagement_event_key`, sorting
    on the `tuning_start_utc` field and the ranking each of the tuning events
    by start time.  We also count the number of tuning events in each 
    partition and create a forward lag of all of the tuning end times.
    
    Next, we perform datemath to identify whether we have any gaps between the
    panel event start/end times and the tuning event start/end times.  There
    are four kinds of gaps that are flagged:
    
        1. first - we have a gap between the panel event start and the start 
           of the first tuning event
        2. lag - we have a gap between consecutive tuning events, usually due
           to poor coverage in the Nielsen lineup data
        3. last - we have a gap between the last tuning event and the panel
           event end
        4. keep - a placeholder so that we keep all of the original tuning 
           events
           
    Finally, we explode the dataframe to add tuning records for any gaps that
    we found and then we write some business logic to swap the proper tuning 
    start and end times. We will NULL out the program information for all of 
    the gap records in a successive steps.
    
    Args:
    -----
    tuning_events_df: `pyspark.sql.dataframe.Dataframe`, required
        A spark dataframe containing the merge of `metered_tv_location_tv_
        engagement_event` and `program_coded_metered_tv_location_tv_
        engagement_event`
        
    Returns:
    --------
    An expanded Dataframe with tuning coverage for 100% of every panel 
    engagement event
    
    Examples:
    ---------
    TODO: Copy Caleb's examples here
    """
    
    tuning_starts = sorted([ # list of tuning start columns
 
        col for col in tuning_events_df.columns
        if "tuning" in col
        and "_start_" in col
    ])
    
    tuning_ends = sorted([ # List of tuning end columns
        col for col in tuning_events_df.columns
        if "tuning" in col
        and "_end_" in col
    ])
    
    window = Window.partitionBy( # for use in window functions
        "media_engagement_event_key"
    ).orderBy(
        "tuning_start_utc"
    )
    
    # Number of tuning events in each engagement event
    counts = tuning_events_df.groupby(
        "media_engagement_event_key"
    ).count()
    
    # Metrics we need to make the proper time adjustments
    indicators = tuning_events_df.withColumn(
        "lag",
        F.struct(*[
            F.col(col) for col 
            in tuning_starts+tuning_ends
        ])
    ).withColumn(
        "lag",
        F.lag(F.col("lag")).over(window)
    ).withColumn(
        "rank",
        F.row_number().over(window)
    ).join(
        clone_dataframe(counts),
        on="media_engagement_event_key",
        how="inner"
    )
    
    # Identify tuning gaps in engagement events
    gaps = indicators.withColumn(
        "gap",
        F.lit("keep")
    ).withColumn(
        "gap",
        F.when(
            (F.col("rank")==1) & 
            (subtract_timestamps(
                F.col("tuning_start_utc"),
                F.col("event_start_utc")
            ) > 1),
            F.concat(F.col("gap"), F.lit(",first"))
        ).otherwise(
            F.col("gap")
        )
    ).withColumn(
        "gap",
        F.when(
            subtract_timestamps(
                F.col("tuning_start_utc"), 
                F.col("lag").getItem("tuning_end_utc")
            )> 1,
            F.concat(F.col("gap"), F.lit(",lag"))
        ).otherwise(
            F.col("gap")
        )
    ).withColumn(
        "gap",
        F.when(
            (F.col("rank")==F.col("count")) &
            (subtract_timestamps(
                F.col("event_end_utc"), 
                F.col("tuning_end_utc")
            ) > 1),
            F.concat(F.col("gap"), F.lit(",last"))
        ).otherwise(
            F.col("gap")
        )
    )
    
    # Add the gap records
    fills = gaps.withColumn(
        "type",
        F.explode(
            F.split(F.col("gap"), ",")
        )
    )
    
    # Swap times to fill in the gaps
    for s, e in zip(tuning_starts, tuning_ends):
        fills = fills.withColumn( # Stash new tuning start times
            "new_{}".format(s),
            F.when(
                F.col("type")=="keep",
                F.col(s)
            ).otherwise(
                F.when(
                    F.col("type")=="first",
                    F.col(s.replace("tuning", "event"))   
                ).otherwise(
                    F.when(
                        F.col("type")=="lag",
                        timestamp_delta(F.col("lag").getItem(e), 1)
                    ).otherwise(
                        F.when(
                            F.col("type")=="last",
                            timestamp_delta(F.col(e), 1)
                        )
                    )
                )
            )
        ).withColumn( # Stash new tuning end times
            "new_{}".format(e),
            F.when(
                F.col("type")=="keep",
                F.col(e)
            ).otherwise(
                F.when(
                    F.col("type")=="first",
                    timestamp_delta(F.col(s), -1)
                ).otherwise(
                    F.when(
                        F.col("type")=="lag",
                        timestamp_delta(F.col(s), -1)
                    ).otherwise(
                        F.when(
                            F.col("type")=="last",
                            F.col(e.replace("tuning", "event"))
                        )
                    )
                )
            )
        ).withColumn( # Make the swap on start times
            s,
            F.col("new_{}".format(s))
        ).withColumn( # Make the swap on end times
            e,
            F.col("new_{}".format(e))
        ).drop( # Drop the stash columns
            "new_{}".format(s),
            "new_{}".format(e)
        )
               
    return fills.drop("lag")
   
    
def event_pull(spark, household_intab, start_date, end_date):
    """
    Pulls all intab tuning events covering a specified date range. Note that 
    the events table/view is quite large so we make use of a partitioning
    column to speed up the query. 
    
    We apply an offset of 30 days on the `start_date` and `end_date` and 
    only accept `collection_period_date` values that fall within this range. 
    This is a very conservative way to ensure that we do not need to query the 
    entire events table to get the data we need.
    
    Args:
    -----
    spark: `pyspark.sql.session.SparkSession`, required
        A Pyspark session we can use to query the MDL
    start_date: `str`, required
        Date string in YYYY-mm-dd format used to subset events
    end_date: `str`, required
        Date string in YYYY-mm-dd format used to subset events
          
    Returns:
    --------
    A `pypsark.sql.dataframe.DataFrame` containing all of the selected tuning
    events
    """
    
    # Apply a conservative offset to the start and end dates
    collection_start = int(time_shift(start_date, -30))
    collection_end = int(time_shift(end_date, 30))
    
    events = spark.sql(
        "SELECT media_engagement_event_key, "
               "intab_period_id, "
               "credit_start_of_viewing_utc as event_credit_start_utc, "
               "credit_end_of_viewing_utc as event_credit_end_utc, " 
               "viewed_start_of_viewing_local_time as event_start_local, "
               "viewed_end_of_viewing_local_time as event_end_local, "
               "viewed_start_of_viewing_utc as event_start_utc, "
               "viewed_end_of_viewing_utc as event_end_utc, "
               "national_service_time_zone_id as time_zone_id, "
               "viewed_collection_period_id, "
               "household_media_consumer_id, "
               "location_media_consumer_id, "
               "source_installed_location_number, "
               "media_device_id, "
               "source_media_device_number, "
               "metered_tv_credit_station_code, "
               "metered_tv_media_credit_id, "
               "market_id, "
               "metro_area_market_id, "
               "demographic_control_area_id "
        "FROM tam_npm_mch_tv_prod.metered_tv_location_tv_engagement_event "
        "WHERE released_for_national_processing_flag = 'Y' "
        "AND metered_tv_credit_station_code NOT IN "
        "(1079, 4619, 4999, 5000, 5001, 5002, 5003, 5004, 5005, 5006, 5007, 5008, 5009, 8678, 9094) "
        "AND streaming_indicator != 'Y' "
        "AND collection_period_date BETWEEN {start} AND {end}".format(
            start=collection_start,
            end=collection_end
        )
    )
            
    tuning = events.join( # Keep only events falling within our date range
        household_intab,
        on=[
            "intab_period_id",
            "household_media_consumer_id",
            "location_media_consumer_id"
        ],
        how="inner"
    )
    
    tuning = cast_timestamp(tuning, [
        "event_credit_start_utc",
        "event_credit_end_utc",
        "event_start_local", 
        "event_end_local",
        "event_start_utc",
        "event_end_utc",    
    ])
    
    return tuning


#TODO Move this function into vam module
def make_dayparts(df):
    """
    Calculate event durations and dayparts on a spark dataframe
    
    Args:
    -----
    df: `pyspark.sql.dataframe.DataFrame`, required
        A data frame of tuning events; the output of the `pull_data` function
    
    Returns:
    --------
    A `pyspark.sql.dataframe.DataFrame` containing formatted tuning events
    """
    
    dayparts = df.withColumn(
        "start_hour",
        F.hour(F.col("event_start_local"))
    ).withColumn(
        "daypart",
        calculate_daypart("week_number", "start_hour")
    ).drop(
        "start_hour",
    )

    return dayparts
