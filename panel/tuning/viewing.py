from pyspark.sql import Window
import pyspark.sql.functions as F

from ..utils import time_shift, set_column_value
from ..utils import cast_timestamp, subtract_timestamps
    

def merge_tuning_with_viewing(tuning_df, viewing_df):
    """
    Helper function to merge viewing events onto tuning events. Because this
    is a range join, we have to write the join a bit awkwardly and this 
    function allows us to perform the column renaming and selection in one
    convenient place.
    
    Args:
    -----
    tuning_df: `pyspark.sql.dataframe.DataFrame`, required
        A spark dataframe containing tuning events
    viewing_df: `pyspark.sql.dataframe.DataFrame`, required
        A spark dataframe containing viewing events
        
    Returns:
    --------
    A spark dataframe containing merged tuning and viewing events
    """
    
    pairs = [ # If tuning times are null fall back on event times   
        ("tuning_start_local", "event_start_local"),
        ("tuning_end_local", "event_end_local"),
        ("tuning_start_utc", "event_start_utc"),
        ("tuning_end_utc", "event_end_utc"),
        ("tuning_credit_start_utc", "event_credit_start_utc"),
        ("tuning_credit_end_utc", "event_credit_end_utc")
    ]
    
    for a, b in pairs:
        tuning_df = set_column_value(tuning_df, a, b)
        
    # Add an ID column so we can sort through the rubble of this merge later
    viewing_df = viewing_df.withColumn(
        "vid",
        F.md5(
            F.concat(
                "household_media_consumer_id",
                "person_media_consumer_id",
                "media_device_id",
                "metered_tv_credit_station_code",
                "metered_tv_media_credit_id",
                "viewing_start_utc",
                "viewing_end_utc"
            )
        )
    )
        
    join_fields =  [
        "intab_period_id",
        "market_id",
        "household_media_consumer_id",
        "location_media_consumer_id",
        "source_installed_location_number",
        "media_device_id",
        "source_media_device_number",
        "metered_tv_credit_station_code",
        "metered_tv_media_credit_id",
        "viewed_collection_period_id",
    ]
                    
    # Merge tuning and viewing
    merge_fields = []
    
    for field in join_fields:
        # Rename viewing columns to avoid duplicate column names post-merge
        viewing_df = viewing_df.withColumnRenamed(
            field,
            "v_{}".format(field)
        )
        
        # Wonky way of writing a range join in pyspark
        merge_fields.append(
            tuning_df[field]==viewing_df["v_{}".format(field)]
        )    
        
    # TODO Figure out how to add range_join hint for qrtr hrs (Spark 2.4.4+)
    tuning_with_viewing = tuning_df.join(
        viewing_df,
        on=merge_fields + [ # merge conditions to detect ANY overlap 
            tuning_df.tuning_start_utc<=viewing_df.viewing_end_utc,
            tuning_df.tuning_end_utc>=viewing_df.viewing_start_utc
        ],
        how="left"
    ).select(
        tuning_df.columns + [
            col for col in viewing_df.columns
            if not col.startswith("v_")
        ]
    )
    
    return tuning_with_viewing


def condense_viewing(viewing_df):
    
    partition_cols = [
        "intab_period_id",
        "household_media_consumer_id",
        "location_media_consumer_id",
        "person_media_consumer_id",
        "media_device_id",
        "metered_tv_credit_station_code",
        "metered_tv_media_credit_id",
    ]
    
    time_cols = [
        col for col in viewing_df.columns
        if col.startswith("viewing")
    ]
    
    other_cols = list(
        set(viewing_df.columns) - 
        set(partition_cols + time_cols)
    )
    
    # we have a small minority of records with overlapping viewing times
    overlaps_removed = viewing_df.withColumn(
        "lag",
        F.lag(
            F.col("viewing_end_utc")
        ).over(
            Window.partitionBy(
                *partition_cols
            ).orderBy(
                "viewing_start_utc"
            )
        )
    ).filter( # keep an event if not encompassed by the previous event
        (F.col("lag") <= F.col("viewing_end_utc")) |
        (F.col("lag").isNull()) # or if it is the first event in a sequence
    )
    
    back_to_back = overlaps_removed.withColumn(
        "lag",
        F.lag( # recalculate the lag so we end up with good sequences
            F.col("viewing_end_utc")
        ).over(
            Window.partitionBy(
                *partition_cols
            ).orderBy(
                "viewing_start_utc"
            )
        )
    ).withColumn(
        "sequence",
        F.sum(
            F.when(
                subtract_timestamps(
                    F.col("viewing_start_utc"),
                    F.col("lag")
                ) > 60,
                F.lit(1)
            ).otherwise(
                F.lit(0)
            )
        ).over(
            Window.partitionBy(
                *partition_cols
            ).orderBy(
                "viewing_start_utc"
            )
        )
    )
    
    start_cols = [
        F.min(F.col(col)).alias(col) for col in time_cols 
        if "_start_" in col
    ]
    
    end_cols = [
        F.max(F.col(col)).alias(col) for col in time_cols
        if "_end_" in col
    ]
    
    other_cols = [
        F.first(F.col(col)).alias(col)
        for col in list(
            set(viewing_df.columns) - 
            set(partition_cols + time_cols)
        )
    ]
    
    consecutive = back_to_back.groupBy(
        partition_cols + ["sequence"]
    ).agg(
        *(start_cols + end_cols + other_cols)
    ).drop(
        "sequence"
    ).filter(
        subtract_timestamps( # we need at least 2 minutes of viewing to keep an event
            F.col("viewing_end_utc"),
            F.col("viewing_start_utc")
        ) >= 60
    )
    
    return consecutive.select(viewing_df.columns)
    
    
def viewing_pull(spark, demo_df, start_date, end_date):
    """
    Pulls all intab viewing events covering a specified date range. This tells
    us who in the home was watching a given device-level tuning event. Note 
    that the events table/view is quite large so we make use of a partitioning
    column to speed up the query. 
    
    We apply an offset of 30 days on the `start_date` and `end_date` and 
    only accept `collection_period_date` values that fall within this range. 
    This is a very conservative way to ensure that we do not need to query the 
    entire events table to get the data we need.
    
    Args:
    -----
    spark: `pyspark.sql.session.SparkSession`, required
        A Pyspark session we can use to query the MDL
    household_intab:
    person_intab:
    
    Returns:
    --------
    A `pypsark.sql.dataframe.DataFrame` containing all of the selected viewing
    events
    """

    # Apply a conservative offset to the start and end dates
    collection_start = int(time_shift(start_date, -30))
    collection_end = int(time_shift(end_date, 30))
    
    event = spark.sql(
        "SELECT media_engagement_event_key as person_media_engagement_event_key, "
               "credit_start_of_viewing_utc as viewing_credit_start_utc, "
               "credit_end_of_viewing_utc as viewing_credit_end_utc, "       
               "viewed_start_of_viewing_local_time as viewing_start_local, "
               "viewed_end_of_viewing_local_time as viewing_end_local, "
               "viewed_start_of_viewing_utc as viewing_start_utc, "
               "viewed_end_of_viewing_utc as viewing_end_utc, "
               "market_id, "
               "person_media_consumer_id, "
               "location_media_consumer_id, "
               "household_media_consumer_id, "
               "source_installed_location_number, "
               "intab_period_id, "
               "media_device_id, "
               "source_media_device_number, "
               "metered_tv_credit_station_code, "
               "metered_tv_media_credit_id, "
               "viewed_collection_period_id "
        "FROM tam_npm_mch_tv_prod.metered_tv_location_person_tv_engagement_event "
        "WHERE released_for_national_processing_flag = 'Y' "
        "AND streaming_indicator != 'Y' "
        "AND metered_tv_credit_station_code NOT IN "
        "(1079, 4619, 4999, 5000, 5001, 5002, 5003, 5004, 5005, 5006, 5007, 5008, 5009, 8678, 9094) "
        "AND collection_period_date BETWEEN {start} AND {end}".format(
            start=collection_start,
            end=collection_end
        )
    ).drop_duplicates([
        "intab_period_id",
        "household_media_consumer_id",
        "location_media_consumer_id",
        "person_media_consumer_id",
        "media_device_id",
        "viewing_start_local",
        "viewing_end_local"
    ])
        
    viewing = event.join( # Only pull intab panelists
        demo_df,
        on=[
            "intab_period_id",
            "household_media_consumer_id",
            "location_media_consumer_id",
            "person_media_consumer_id"
        ],
        how="inner"
    )
    
    viewing = cast_timestamp(viewing, [
        "viewing_credit_start_utc",
        "viewing_credit_end_utc",
        "viewing_start_local", 
        "viewing_end_local",
        "viewing_start_utc",
        "viewing_end_utc",    
    ])

    return condense_viewing(viewing)
