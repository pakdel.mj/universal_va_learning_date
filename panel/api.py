from . import query
import pyspark.sql.functions as F


def get_viewing_data(
    spark,
    start_date,
    end_date,
    collection_method="hybrid"
):
    """
    Environment agnostic driver function to create panel viewing. All that's 
    needed is a Spark connection to the MDL and a date range. This is meant to 
    become the standard for creating training data for any kind of viewer 
    assignment model.
    
    Args:
    -----
    spark: `pyspark.sql.session.SparkSession`, required
        A spark session we can use to connect to the MDL
    start_date: `str`, required
        Date string in YYYY-mm-dd format defining the start of the range
    end_date: `str`, required
        Date string in YYYY-mm-dd format defining the end of the range
    collection_method: `string`, optional
        If set to `hybrid` we will include all tuning events originating from 
        people meters; if `npm` we will include only tuning events from NPM
        households; if `tvpc` we will only include events from the TV/PC 
        panel; and if `all` we will return all intab events (including 
        vampires)
        
    Returns:
    --------
    A spark dataframe containing official Nielsen viewing events from the NPM
    panel
    """    
    
    household_intab = query.make_household_intab(
        spark,
        start_date,
        end_date,
        collection_method
    )
        
    person_intab = query.make_person_intab(
        spark, 
        start_date,
        end_date
    )
    
    demo_df = query.make_demos(
        spark,
        household_intab,
        person_intab,
        end_date
    )
    
    dma_ids = query.make_dma_ids(spark)
        
    viewing_df = query.make_viewing(
        spark, 
        demo_df,
        start_date, 
        end_date
    ).join(
        dma_ids,
        on="market_id",
        how="left"
    )
    
    return viewing_df
    
    

def get_panel_data( 
    spark, 
    start_date, 
    end_date, 
    collection_method="hybrid",
    viewed_events_only=False, 
    remove_single_person_homes=False
):
    """
    Environment agnostic driver function to create panel data. All that's 
    needed is a Spark connection to the MDL and a date range. This is meant to 
    become the standard for creating training data for any kind of viewer 
    assignment model.
    
    Args:
    -----
    spark: `pyspark.sql.session.SparkSession`, required
        A spark session we can use to connect to the MDL
    start_date: `str`, required
        Date string in YYYY-mm-dd format defining the start of the range
    end_date: `str`, required
        Date string in YYYY-mm-dd format defining the end of the range
    collection_method: `string`, optional
        If set to `hybrid` we will include all tuning events originating from 
        people meters; if `npm` we will include only tuning events from NPM
        households; if `tvpc` we will only include events from the TV/PC 
        panel; and if `all` we will include all intab events (including 
        vampires)
    viewed_events_only: `boolean`, optional
        Flag to indicate whether or not we should return all tuning events or
        only those where person-level viewing has been assigned
    remove_single_person_homes: `boolean`, optional
        If `True` we will subset to households with more than 1 person and 
        where device informatioon is present, otherwise if `False` we will keep 
        all tuning events regardless of device status or household size
        
    Returns:
    --------
    A spark dataframe containing official Nielsen tuning events from the NPM
    panel
    """
    
    household_intab = query.make_household_intab(
        spark,
        start_date,
        end_date,
        collection_method
    ).cache()
    
    person_intab = query.make_person_intab(
        spark, 
        start_date,
        end_date
    )
    
    demo_df = query.make_demos(
        spark, 
        household_intab, 
        person_intab,
        end_date
    ).cache()
    
    dma_df = query.make_dma_ids(spark)
        
    event_df = query.make_events(
        spark,
        household_intab,
        start_date, 
        end_date,
    )
    
    coded_df = query.make_coded_events(
        spark, 
        event_df,
        start_date, 
        end_date
    )
    
    genre_df = query.make_event_genre(
        spark, 
        coded_df
    )
    
    tuning_df = query.make_tuning_events(
        event_df,
        genre_df
    )
    
    viewing_df = query.make_viewing(
        spark, 
        demo_df,
        start_date, 
        end_date
    )
    
    panel_df = query.make_panel_events(
        dma_df,
        tuning_df,
        viewing_df
    )
    
    device_df = None
    
    aligned_df = query.align_panel_data(
        demo_df,
        panel_df,
        device_df, 
        remove_single_person_homes,
        viewed_events_only
    )
    
    return aligned_df