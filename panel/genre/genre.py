import pyspark.sql.types as T
import pyspark.sql.functions as F
from pyspark.sql import Window

from ..utils import time_shift, cast_timestamp
from .conditions import rename_genre


@F.udf(T.ArrayType(T.MapType(T.StringType(), T.LongType())))
def flag_overlap(list_of_timestamps):
    """
    Custom UDF to deduplicate tuning events from the `program_coded` tables.
    We first sort the data on various fields, such that events with lower
    verified codes are favored over those with higher verified codes. This
    helps us select the clean record as opposed to the duplicated record for 
    cases where we have panelists credited to two programs or the same program
    simultaneously. 
    
    We also favor LIVE events over NORMAL events and events from NETWORK/CABLE
    over those from SYNDICATED originator types. In a perfect world, we would
    be able to apply a filter (on the verified codes?) to knock these out, but
    unless more information is forthcoming this search algorithm is our best
    bet at keeping the tuning events as grounded in reality as possible.
    
    Args:
    -----
    list_of_timestamps: `List[Map]`, required
        A list containing all of the `program_coded` events for a given 
        `media_engagement_event_id`. Each element of the list is a dictionary
        containing the fields we are sorting on.
        
    Returns:
    --------
    An array of MapTypes containing the ids and flags for each 
    `media_engagement_event_key` and `program_coded_media_engagement_event_key`
    which can then be exploded back to row format.
    """
    
    lot = sorted(list_of_timestamps, key=lambda x: (
        x["tuning_start_local"].timestamp(), # Sort first by start date
        x["event_verified_code"], # Try to avoid verified code = 18
        0 if x["is_live"]=="Y" else 1, # Favor live events
        x["originator_type_code"], # Favor Network over Syndicated
        -x["tuning_end_local"].timestamp() # Favor less fragmented events
    ))

    end = lot[0]["tuning_start_local"]

    overlap = []
    
    # A simple search algorithm to detect overlapping timestamps
    for i, row in enumerate(lot):
        if ( # if this row starts before a previous end time
          end - row["tuning_start_local"]
        ).total_seconds() > 0:
            overlap.append({ # We have overlap
                "key": row["program_coded_media_engagement_event_key"],
                "rol": row["report_originator_lineup_key"],
                "row": i,
                "flag": 1
            })
        else:
            overlap.append({ # We have no overlap
                "key": row["program_coded_media_engagement_event_key"],
                "rol": row["report_originator_lineup_key"],
                "row": i,
                "flag": 0
            })

            # Update the saved end time
            end = max(end, row["tuning_end_local"])
        
    return overlap # return a list of dicts


def make_flags(coded_tuning):
    """
    Function to aggregate and collect all of the incidents for a given 
    `media_engagement_event_key`. The output of `collect_list` is then sent
    directly into the search algorithm whose output is then exploded back to
    a workable dataframe format. This table can then be merged up with the 
    input dataframe so that all of the duplicates detected by the search
    algorithm can easily be dropped.
    
    Args:
    -----
    coded_tuning: `pyspark.sql.dataframe.DataFrame`, required
        A Pyspark dataframe containing the output of the `get_coded_events`
        function.
        
    Returns:
    --------
    A spark dataframe containing the IDs and a duplication flag for all of 
    the tuning events that were passed through the search algorithm. 
    """
    
    flags = coded_tuning.groupby(
        "media_engagement_event_key"
    ).agg(
        F.collect_list( # A list of all events for each key
            F.struct( # A map of all the fields we need for each event
                F.col("program_coded_media_engagement_event_key"),
                F.col("report_originator_lineup_key"),
                F.col("tuning_start_local"),
                F.col("tuning_end_local"),
                F.col("is_live"),
                F.col("originator_type_code"),
                F.col("event_verified_code")
            )
        ).alias("timestamps")
    ).withColumn(
      "flag",
      F.explode( # Convert back to row format
        flag_overlap(F.col("timestamps")) # run the search algorithm
      )
    ).select( # Convert map field back to columns
      "media_engagement_event_key",
      F.col("flag")["key"].alias("program_coded_media_engagement_event_key"),
      F.col("flag")["rol"].alias("report_originator_lineup_key"),
      F.col("flag")["row"].alias("event_order"),
      F.col("flag")["flag"].alias("is_duplicated").cast("boolean")
    ).cache()
    
    return flags


def adjust_tuning_times(coded_events):
    """
    We have observed several records with start times of the form xx:xx:59 and 
    end times of the form xx:xx:58.  This function corrects these errors 
    (which are present in the MDL tables) and converts the tuning times to 
    their expected xx:xx:00 and xx:xx:59 formats.
    
    Args:
    -----
    coded_events: `pyspark.sql.dataframe.DataFrame`, required
        A Spark dataframe containing tuning start and end times
        
    Returns:
    A dataframe with tuning start and end times in the expected format.
    """
    
    for col in [
        "tuning_credit_start_utc",
        "tuning_start_utc",
        "tuning_start_local"
    ]:
        coded_events = coded_events.withColumn(
            col,
            (F.floor( # round start times down to nearest minute
                F.unix_timestamp(
                    F.col(col)
                ) / 60
            ) * 60).cast("timestamp")
        )
        
    for col in [
        "tuning_credit_end_utc",
        "tuning_end_utc",
        "tuning_end_local"
    ]:
        coded_events = coded_events.withColumn(
            col,
            (F.ceil( # round end times up to nearest minute
                F.unix_timestamp(
                    F.col(col)
                ) / 60
            ) * 60 - 1).cast("timestamp") # subtract one second
        )
        
    return coded_events
        

def get_coded_events(spark, start_date, end_date):
    """
    Function to pull program-level information for all of the events returned
    by the `event_pull` function. Note that this table is national-only, so 
    no local content is included. This means we do not have any coding for 
    local news, as well as shows like Ellen and Wheel of Fortune. Ratings for
    these programs are not reported nationally and are only available via 
    Nielsen's LPM measurement.
    
    Args:
    -----
    spark: `pyspark.sql.session.SparkSession`, required
        A Pyspark session we can use to query the MDL
    start_date: `str`, required
        Date string in YYYY-mm-dd format used to subset events
    end_date: `str`, required
        Date string in YYYY-mm-dd format used to subset events
        
    Returns:
    --------
    A `pyspark.sql.dataframe.DataFrame` containing all of the tuning events
    that can be coded by Nielsen. Note that these results will contain
    overlapping events.
    """

    collection_start = int(time_shift(start_date, -30))
    collection_end = int(time_shift(end_date, 30))
    
    coded = spark.sql(
        "SELECT program_coded_media_engagement_event_key, "
               "media_engagement_event_key, "
               "report_originator_lineup_key, "
               "household_media_consumer_id, "
               "location_media_consumer_id, "
               "media_device_id, "
               "media_device_key, "
               "source_installed_location_number, "
               "source_media_device_number, "
               "metered_tv_credit_station_code, "
               "metered_tv_media_credit_id, "
               "time_shifted_viewing_type_code as time_shift_code, "
               "seconds_from_record_to_playback as time_shift_seconds, "
               "credit_start_of_viewing_utc as tuning_credit_start_utc, "
               "credit_end_of_viewing_utc as tuning_credit_end_utc, "
               "viewed_start_of_viewing_local_time as tuning_start_local, "
               "viewed_end_of_viewing_local_time as tuning_end_local, "
               "viewed_start_of_viewing_utc as tuning_start_utc, "
               "viewed_end_of_viewing_utc as tuning_end_utc, "
               "distribution_event_verified_state_code as event_verified_code "
        "FROM tam_npm_mch_tv_prod.program_coded_metered_tv_location_media_engagement_event "
        "WHERE released_for_processing_flag = 'Y' "
        "AND lineup_release_type_code = 1 "
        "AND metered_tv_credit_station_code NOT IN "
        "(1079, 4619, 4999, 5000, 5001, 5002, 5003, 5004, 5006, 5007, 5008, 5009, 8678) "
        "AND collection_period_date BETWEEN {start} AND {end}".format(
            start=collection_start,
            end=collection_end
        )
    ).withColumn( 
        "row_number",
        F.row_number().over( # Arduous deduplication routine 
            Window.partitionBy( # because nothing comes easy on the MDL
                "media_engagement_event_key", 
                "tuning_start_local",
                "tuning_end_local"
            ).orderBy(
                F.col("event_verified_code") # Keep lower verified codes over higher
            )
        )
    ).filter( # Only keep the event with the lowest verified code
        F.col("row_number")==1
    )
    
    coded = cast_timestamp(coded, [
        "tuning_credit_start_utc",
        "tuning_credit_end_utc",
        "tuning_start_local", 
        "tuning_end_local",
        "tuning_start_utc",
        "tuning_end_utc",    
    ])
        
    return adjust_tuning_times(coded)


def genre_pull(spark):
    """
    Function to pull genre information from the MDL. All of the `program_coded`
    tables contain a `report_originator_lineup_key` which can be joined with
    the `report_originator_lineup` table to bring in Nielsen program names and
    genres. 
    
    Args:
    -----
    spark: `pyspark.sql.session.SparkSession`, required
        A Pyspark session we can use to query the MDL
    
    Returns:
    --------
    A spark dataframe containing the program and genre information for all 
    available `report_originator_lineup_keys`
    """
    
    rol = spark.sql("""
        SELECT report_originator_lineup_key,
               originator_type_code,
               program_name,
               is_live_broadcast_flag as is_live,
               lineup_type_code,
               reprocessing_type_code,
               airing_feed_pattern_code as feed_pattern_code,
               report_start_time_ny,
               report_end_time_ny,
               original_duration_minutes as total_program_duration,
               national_tv_broadcast_date as broadcast_start_date,
               program_summary_type_code,
               program_expanded_type_code,
               creation_datetime
        FROM tam_npm_mch_tv_prod.report_originator_lineup
        WHERE released_for_processing_flag = 'Y'
        AND originator_type_code <> 21
        AND program_summary_type_code IS NOT NULL
        AND lineup_release_type_code = 1
    """).withColumn(
        "broadcast_end_date", # Adjust for programs that cross midnight
        F.when( # Some clients put the broadcast date as their local time (!!!)
         F.col("report_start_time_ny") > F.col("report_end_time_ny"),
            F.from_unixtime(
                F.unix_timestamp(
                    F.col("broadcast_start_date").cast("date")
                ) + 24*60*60,
                "YYYY-MM-dd"
            )
        ).otherwise(
            F.col("broadcast_start_date")
        )
    ).withColumn( # Convert start time to timestamp
        "report_start_time_ny",   
        F.concat(
            F.col("broadcast_start_date"), 
            F.lit(" "), 
            F.col("report_start_time_ny")
        ).cast("timestamp")
    ).withColumn( # Convert end time to timestamp
        "report_end_time_ny",
        F.concat(
            F.col("broadcast_end_date"),
            F.lit(" "),
            F.col("report_end_time_ny")
        ).cast("timestamp")
    )

    summary = spark.sql("""
        SELECT program_summary_type_code,
               program_summary_type_desc
        FROM tam_npm_mch_tv_prod.program_summary_type
    """)
    
    genre_info = rol.join(
        summary, 
        on="program_summary_type_code",
        how="left"
    ).withColumn(
        "genre",
        rename_genre("program_summary_type_desc")
    ).drop(
        "program_summary_type_code",
        "program_summary_type_desc"
    )
    
    return genre_info
