import itertools
import pyspark.sql.functions as F


def rename_genre(colname):
    """
    Function to standardize and sanitize base Nielsen genres.
    """
    return F.when(
        F.col(colname).isin([
            "CHILD MULTI-WEEKLY", 
            "CHILDREN'S NEWS", 
            "CHILD DAY - ANIMATION", 
            "CHILD EVENING", 
            "CHILD - LIVE"
        ]),
        F.lit("CHILDRENS")
    ).when(
        F.col(colname)=="POLITICAL",
        F.lit("PAID POLITICAL")
    ).when(
        F.col(colname).isin([
            "FORMAT VARIES", 
            "UNCLASSIFIED"
        ]),
        F.lit("ALL")
    ).when(
        F.col(colname).isNull(),
        F.lit("ALL")
    ).otherwise(
        F.col(colname)
    )


def collapse_genre(df):
    """
    Function to collapse sanitized Nielsen genres down to the expected VAM 
    genres.
    """
    
    collapsed_genres = {
        "CHILDRENS"                 : "Childrens",
        "COMEDY VARIETY"            : "Comedy",
        "EVENING ANIMATION"         : "Comedy",
        "SITUATION COMEDY"          : "Comedy",
        "DAYTIME DRAMA"             : "Drama", 
        "GENERAL DRAMA"             : "Drama",
        "MUSICAL DRAMA"             : "Drama",
        "OFFICIAL POLICE"           : "Drama",
        "PRIVATE DETECTIVE"         : "Drama",
        "SCIENCE FICTION"           : "Drama",
        "SUSPENSE/MYSTERY"          : "Drama",
        "WESTERN DRAMA"             : "Drama",
        "FEATURE FILM"              : "Film",
        "CONVERSATIONS, COLLOQUIES" : "News", 
        "GENERAL DOCUMENTARY"       : "News", 
        "INSTRUCTION, ADVICE"       : "News", 
        "NEWS"                      : "News",
        "NEWS DOCUMENTARY"          : "News",
        "PAID POLITICAL"            : "News",
        "SPORTS ANTHOLOGY"          : "Sports", 
        "SPORTS COMMENTARY"         : "Sports", 
        "SPORTS EVENT"              : "Sports", 
        "SPORTS NEWS"               : "Sports",
        "ADVENTURE"                 : "Variety", 
        "AUDIENCE PARTICIPATION"    : "Variety", 
        "AWARD CEREMONIES"          : "Variety", 
        "CONCERT MUSIC"             : "Variety",
        "DEVOTIONAL"                : "Variety", 
        "GENERAL VARIETY"           : "Variety", 
        "PARTICIPATION VARIETY"     : "Variety", 
        "POPULAR MUSIC"             : "Variety",
        "POPULAR MUSIC STANDARD"    : "Variety",
        "QUIZ GIVE AWAY"            : "Variety", 
        "QUIZ PANEL"                : "Variety",
        "ALL"                       : "All"
    }
    
    mapping_expr = F.create_map([
        F.lit(x) for x in itertools.chain(*collapsed_genres.items())
    ])
    
    return  mapping_expr.getItem(F.col("genre"))
