# Overview

This module is meant to be the go-to source for Nielsen associates who wish to pull tuning events from the NPM panel on the MDL.  We have seen several intrepretations of this procedure floating around Nielsen and we are hoping this codebase can represent the very first steps towards developing a standard process that can be used across Data Science.  Although we do not claim to be the experts, a lot of detective work has gone into this codebase and several teams within Data Science have contributed insights, knowledge and/or validation to this effort.  

The procedure in its current state is derived from the data preparation steps contained in the orginal DAR/DCR Viewer Assignment Model.  The code has been converted from multiple SQL queries with several improvements made along the way. Most notably, we are automatically enforcing the removal of Viewer Assigned Panelists (_vampires_) and we are performing a more nuanced deduplication of coded tuning events compared to the original version that we inherited.  

If you are working on or have ever worked with NPM tuning events, we would welcome any feedback or critiques of the methods and functions contained in this repository.  In addition, if you are doing any work on researching or re-implementing a pre-existing Viewer Assignment methodology, we would be happy to speak with you about the potential application of this module to the data preparation phase of your particular implementation.  
    
# Requirements

The code has been designed in such a way that it is environment agnostic and can be run across both Databricks and Intelligence Studio.  As long as you have a Spark cluster that can connect to the MDL, you can run this code.  

We have tested and successfully executed this code on Spark clusters with at least the following specifications:

* Python 3.7
* Spark 2.3+
* Worker Memory (RAM): 86 GB

As always, more workers should result in faster run times but your mileage may vary.  In general, we are able to produce our output in about 30-40 minutes on the clusters we have tested on.  Most of this is due to the various deduplication steps we must perform to remove _vampires_ and overlapping tuning events.

# Usage

For convenience, we have included a driver function that can be easily imported.  The only parameters that are required are the `spark` session you are using and a `start_date` and an `end_date` to define the date range you wish to focus on.  An example is given below:

```
from magic.panel import get_panel_data

panel = get_panel_data(
    spark,
    start_date="2019-02-01", 
    end_date="2019-07-31", 
    collection_method="hybrid",
    viewed_events_only=False, 
    remove_single_person_homes=False
)

panel.write.format("parquet").save("s3://your-bucket-here")
panel = spark.read.format("parquet").load("s3://your-bucket-here")
```
The `collection_method` parameter is used to further subset the panel to NPM homes only (most are hybrid NPM/LPM) or to TVPC panel homes only.  In general, we recommend keeping this set to `hybrid` to return all intab tuning events originating from people meters, Nielsen's most accurate form of measurement.

The `viewed_events_only` flag is useful if you wish to narrow down the output further.  The process is currently designed such that we copy all of the device-level tuning events to all persons in the household, regardless of whether or not they were actually listed as viewing the event in our person-level viewing tables.  Performing the merge in this way allows us to calculate probability lookups in the downstream DAR/DCR VAM module (a kind of "opportunity cost of viewing").  If you are only interested in tuning events where a person was present, you should set this flag to `True`.  

The `remove_single_person_homes` parameter can be set to `True` if you do not wish to include any household sizes of 1 in your final learning data.  One of the quirks of viewer assignment is that we do not actually have to run the model if there is only one person present in a home (we just assume that this person is always the one who is watching).  If you change this parameter to `False`, all of these homes will be kept in the final output.

# Output

Throughout this project, our goal has been to keep the output of this module as generic as possible so that it is applicable to as many different use cases as possible.  There are literally hundreds of fields available on the MDL (with many of them being redundant) so if we have missed any columns that are especially important, please let us know and we can easily add them.

Use this [link](docs/panel_sample.csv) to access a sample extract of the output of this module.  Please refer to Alation for a detailed explanation of each of the fields.