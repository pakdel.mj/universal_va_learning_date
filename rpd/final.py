# Databricks notebook source
import pyspark.sql.functions as F

# COMMAND ----------

start_date = '2019-07-14'
end_date = '2019-07-14'


hhno = 30831785 
stn = 10214
all = False

# COMMAND ----------

tgb2_primary_columns = [
  'sample_characteristic_intab_period_id',
  'legacy_household_id',
  'global_household_id',
  'dma_code',
  'time_zone_code',
  'tv_and_pc_ind']
tgb2_secondary_columns = [
  'npm_sample_ind',
  'code_reader_sample_ind',
  'rpd_sample_ind',
  'hh_asian_ind',
  'hh_black_ind',
  'language_class_code',
  'number_of_persons',
  'hoh_origin_code',
  'number_of_viewable_sets',
]

b1010_columns =[
  'intab_date',
  'legacy_household_id',
  'global_household_id',
  'PERSON_NUMBER',
  'DMA_CODE',
  'AGE',
  'GENDER',
  'SHORT_TERM_VISITOR_FLAG'
  
]

# COMMAND ----------

def get_calendar_dates(spark, start_date, end_date):
    """
    Fetch the appropriate intab dates for a given date range. Keeping this 
    table cached allows us to quickly trim the fat out of all of our other
    data merges.
    
    Args:
    -----
    spark: `pyspark.sql.session.SparkSession`, required
        A Pyspark session we can use to query the MDL
    start_date: `str`, required
        Date string in YYYY-mm-dd format defining the start of the range
    end_date: `str`, required
        Date string in YYYY-mm-dd format defining the end of the range
        
    Returns:
    --------
    A `pyspark.sql.dataframe.DataFrame` containing the selected intab dates
    """
    
    calendar = spark.sql(
        "SELECT calendar_date_id as intab_period_id, "
               "calendar_date, "
               "calendar_day_of_week_number as week_number "
        "FROM tam_npm_mch_tv_prod.calendar_date "
        "WHERE calendar_date between '{start}' AND '{end}'".format(
            start=start_date, 
            end=end_date
        )
    )
    
    return calendar

# COMMAND ----------

def rename_columns(df, col_map):
    """Renames columns for each df in df_list according to the col_map

    Args:
        df_list (list of DataFrames): List of DataFrames to change
        col_map (dict): dictionary of column name changes

    Returns:
        List of DataFrames
    """
    return df.select([F.col(c).alias(col_map.get(c, c)) for c in df.columns])
  
def genre_pull(spark):
    """
    Function to pull genre information from the MDL. All of the `program_coded`
    tables contain a `report_originator_lineup_key` which can be joined with
    the `report_originator_lineup` table to bring in Nielsen program names and
    genres. 
    
    Args:
    -----
    spark: `pyspark.sql.session.SparkSession`, required
        A Pyspark session we can use to query the MDL
    
    Returns:
    --------
    A spark dataframe containing the program and genre information for all 
    available `report_originator_lineup_keys`
    """
    
    rol = spark.sql("""
        SELECT report_originator_lineup_key,
               originator_type_code,
               program_name,
               is_live_broadcast_flag as is_live,
               lineup_type_code,
               reprocessing_type_code,
               airing_feed_pattern_code as feed_pattern_code,
               report_start_time_ny,
               report_end_time_ny,
               original_duration_minutes as total_program_duration,
               national_tv_broadcast_date as broadcast_start_date,
               program_summary_type_code,
               program_expanded_type_code,
               creation_datetime
        FROM tam_npm_mch_tv_prod.report_originator_lineup
        WHERE released_for_processing_flag = 'Y'
        AND originator_type_code <> 21
        AND program_summary_type_code IS NOT NULL
        AND lineup_release_type_code = 1
    """).withColumn(
        "broadcast_end_date", # Adjust for programs that cross midnight
        F.when( # Some clients put the broadcast date as their local time (!!!)
         F.col("report_start_time_ny") > F.col("report_end_time_ny"),
            F.from_unixtime(
                F.unix_timestamp(
                    F.col("broadcast_start_date").cast("date")
                ) + 24*60*60,
                "YYYY-MM-dd"
            )
        ).otherwise(
            F.col("broadcast_start_date")
        )
    ).withColumn( # Convert start time to timestamp
        "report_start_time_ny",   
        F.concat(
            F.col("broadcast_start_date"), 
            F.lit(" "), 
            F.col("report_start_time_ny")
        ).cast("timestamp")
    ).withColumn( # Convert end time to timestamp
        "report_end_time_ny",
        F.concat(
            F.col("broadcast_end_date"),
            F.lit(" "),
            F.col("report_end_time_ny")
        ).cast("timestamp")
    )

    summary = spark.sql("""
        SELECT program_summary_type_code,
               program_summary_type_desc
        FROM tam_npm_mch_tv_prod.program_summary_type
    """)
    
    genre_info = rol.join(
        summary, 
        on="program_summary_type_code",
        how="left"
    )
    
    return genre_info

# COMMAND ----------

def clone_dataframe(df):
    """
    Accepts a `pyspark.sql.dataframe.DataFrame` and returns a clone that can
    be used to circumvent Spark reference errors. Sometimes we need to do this
    when we are merging a child dataframe that was derived directly from a 
    parent dataframe. Renaming the columns (even though we are giving them the
    same names) will force Spark to create a new reference for the child with
    absolutely no computational overhead.
    """
    for col in df.columns:
        df = df.withColumnRenamed(col, col)
    return df

# COMMAND ----------

def get_dma_ids(spark):
    
    dmas = spark.sql("""
        SELECT market_id,
               designated_market_area_id,
               effective_start_date,
               effective_end_date
        FROM tam_npm_mch_tv_prod.designated_market_area
    """).groupby(
        "market_id",
        "designated_market_area_id"
    ).agg(
        F.min("effective_start_date").alias("effective_start_date"),
        F.max("effective_end_date").alias("effective_end_date")
    ).select(
        "market_id",
        "designated_market_area_id"
    )
    
    return dmas
  
def make_dma_ids(spark):
    dma_df = get_dma_ids(spark)
    return dma_df

# COMMAND ----------

def cast_timestamp(df, cols):
    """
    Converts columns to pyspark timestamp format
    """
    for col in cols:
        df = df.withColumn(col, F.from_unixtime(F.col(col),'YYYY-MM-dd HH:mm:ss'))
    return df

# COMMAND ----------

def duration_in_minutes(df,col1,col2,new_col_name):
  df = df.withColumn(new_col_name,
                         F.round((F.unix_timestamp(col1)-F.unix_timestamp(col2))/60)
                        )
  
  return df

# COMMAND ----------

def convert_timestamp(df,cols):
  for col in cols:
    df = df.withColumn(col,F.date_format(col,'yyyy-MM-dd HH:mm:ss') )
  return df

# COMMAND ----------

def overlap_type(df,df1_start,df1_end,df2_start,df2_end):
  
    typed = df.withColumn(
            "swap",
            F.when( # The tuning event completely encompasses the viewing event
                (F.col(df2_start)>=F.col(df1_start)) &
                (F.col(df2_end)<=F.col(df1_end)),
                F.lit("inside")
            ).otherwise(
                F.when(# The viewing event completely encompasses the tuning event
                    (F.col(df2_start)<F.col(df1_start)) &
                    (F.col(df2_end)>F.col(df1_end)),
                    F.lit("outside")
                ).otherwise(
                    F.when( # The viewing event starts before the tuning event 
                        (F.col(df2_start)<F.col(df1_start)) &
                        (F.col(df2_end)<=F.col(df1_end)),
                        F.lit("left")
                    ).otherwise(
                        F.when( # The viewing event ends after the tuning event
                            (F.col(df2_start)>=F.col(df1_start)) &
                            (F.col(df2_end)>F.col(df1_end)),
                            F.lit("right")
                        ).otherwise(
                            F.lit(None)
                        )
                    )
                )
            )
        )
    
    return typed

# COMMAND ----------

def swap_timestamps(typed_df,df1_start,df1_end,df2_start,df2_end):
    
    swap_df = typed_df.withColumnRenamed(df1_start,'tuning_start_utc').withColumnRenamed(df1_end,'tuning_end_utc') \
    .withColumnRenamed(df2_start,'viewing_start_utc').withColumnRenamed(df2_end,'viewing_end_utc').withColumn('n_views',F.lit('1'))
    
    for a, b in [
        ("start_utc", "end_utc")
        
    ]:
    
        swap_df = swap_df.withColumn(
            "v_{}".format(a),
            F.when(
                F.col("swap").isin(["left", "outside"]),
                F.col("tuning_{}".format(a))
            ).otherwise(
                F.col("viewing_{}".format(a))
            )
        ).withColumn(
            "v_{}".format(b),
            F.when(
                F.col("swap").isin(["right", "outside"]),
                F.col("tuning_{}".format(b))
            ).otherwise(
                F.col("viewing_{}".format(b))
            )
        ).withColumn(
            "tuning_{}".format(a),
            F.when(
                F.col("swap").isin(["right", "inside"])&
                (F.col("n_views") == 1),
                F.col("viewing_{}".format(a))
            ).otherwise(
                F.col("tuning_{}".format(a))
            )
        ).withColumn(
            "tuning_{}".format(b),
            F.when(
                F.col("swap").isin(["left", "inside"])& 
                (F.col("n_views") == 1),
                F.col("viewing_{}".format(b))
            ).otherwise(
                F.col("tuning_{}".format(b))
            )
        ).withColumn(
            "viewing_{}".format(a),
            F.col("v_{}".format(a))
        ).withColumn(
            "viewing_{}".format(b),
            F.col("v_{}".format(b))
        )
    
   
    
    
    
    swap_df = swap_df.withColumnRenamed('tuning_start_utc',df1_start).withColumnRenamed('tuning_end_utc',df1_end) \
    .withColumnRenamed('viewing_start_utc',df2_start).withColumnRenamed('viewing_end_utc',df2_end).drop('swap')
    return swap_df.select([
        col for col in swap_df.columns
        if not col.startswith("v_")
    ])
    

# COMMAND ----------

def adj_time(df,df1_start,df1_end,df2_start,df2_end):
  df = df.withColumn(
  df1_start,F.when(F.col(df1_start)<=F.col(df2_start),F.col(df2_start)).otherwise(F.col(df1_start))
                                ) \
.withColumn(
  df1_end,F.when(F.col(df1_end)<=F.col(df2_end),F.col(df1_end)).otherwise(F.col(df2_end))
                                )
  
  return df

# COMMAND ----------

def timestamp_delta(col, n_seconds=59):
    delta = F.unix_timestamp(col) + n_seconds
    return F.date_format(F.to_timestamp(delta),'yyyy-MM-dd HH:mm:ss')   

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 1 : TGB2

# COMMAND ----------

cr_rpd_path = 's3://useast1-nlsn-w-digital-dsci-dev/users/dad8001/tgb2cr_npm_rpd_sep19_071119_071719_hdam.parquet'
# sm_rpd_path = 's3://useast1-nlsn-w-digital-dsci-dev/users/dhni8001/tgb2smm_npm_rpd_sep19_071119_071719_hdam.parquet'

intab = spark.read.parquet(cr_rpd_path) \
.filter('intab_date = "2019-07-14"') \
.filter('dma_code = 632') \
.filter('NATIONAL_DAILY_STATUS_IND in (1,3) OR LOCAL_DAILY_STATUS_IND in (1,3)') \
.select(tgb2_primary_columns)
# .filtr('LOCAL_DAILY_STATUS_IND in (1,3)')
# .filter('HH_BROADBAND_ONLY_IND = "N"')
intab.cache().count()
# display(intab)

# COMMAND ----------

COL_MAP = {
   "sample_characteristic_intab_period_id" : "intab_period_id",
  "tv_and_pc_ind" : "is_tvpc"
}
intab = rename_columns(intab, COL_MAP)
intab = intab.withColumn('household_type',F.lit('recipient'))
display(intab)

# COMMAND ----------

calendar = get_calendar_dates(spark, start_date, end_date)
calendar = rename_columns(calendar, {'calendar_date':'intab_date'})
display(calendar)

# COMMAND ----------

household_intab = intab.join(
        calendar, 
        on="intab_period_id", 
        how="inner"
    )
display(household_intab)

# COMMAND ----------

# MAGIC %md
# MAGIC * 1- should we replace make household_type and get_vampire_codes functions with something to identify rpd common homes
# MAGIC * 2- flag_tvpc function could be eliminated

# COMMAND ----------

# MAGIC %md
# MAGIC # part 2 & 3: b1010

# COMMAND ----------

cr_rpd_path = 's3://useast1-nlsn-w-digital-dsci-dev/data/users/dad8001/Cookbooks/b1010/cr_npm_rpd_071119_071719_hdam.parquet'
# sm_rpd_path = 's3://useast1-nlsn-w-digital-dsci-dev/data/users/dad8001/Cookbooks/b1010/sm_npm_rpd_071119_071719_hdam.parquet'

per_intab = spark.read.parquet(cr_rpd_path) \
.filter('intab_date = "2019-07-14"') \
.filter('dma_code = 632') \
.filter('NATIONAL_DAILY_STATUS_IND in (1,3) OR LOCAL_DAILY_STATUS_IND in (1,3)') \
.select(b1010_columns)
per_intab.cache().count()

# COMMAND ----------

demo_df = per_intab.join( # Combine intab tables
        household_intab,
        on=[
            "intab_date",
            "global_household_id",
            "legacy_household_id"
        ],
        how="inner"
    ).select(per_intab['*'],household_intab['household_type'],household_intab['is_tvpc'],household_intab['week_number'])
demo_df.cache().count()
display(demo_df)

# COMMAND ----------

# MAGIC %md
# MAGIC * instead of computing age in demo_pull function can't we just pull it from the MDL? 

# COMMAND ----------

# MAGIC %md
# MAGIC * validatioin is gonna decide it for us

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 4 : make_dma_ids

# COMMAND ----------

dma_df = make_dma_ids(spark)

# COMMAND ----------

display(dma_df)

# COMMAND ----------

# MAGIC %md
# MAGIC # part 5 : Tuning table

# COMMAND ----------

tuning_ch = spark.sql(''' select * from  tam_stb_igsn_chrtr_dlvry_prod.stb_cnsm_evnt_cppd_with_crdt
where 1=1
and dt_dt between '2019-07-13' and '2019-07-24'
and intb_dt = '2019-07-14'
and designated_market_area_id = 632
and rlsd_fr_prcs_flg = "Y"
and intb_flg = "Y"
''')

tuning_dtv = spark.sql(''' select * from  tam_stb_igsn_dtv_dlvry_prod.stb_cnsm_evnt_cppd_with_crdt
where 1=1
and dt_dt between '2019-07-13' and '2019-07-24'
and intb_dt = '2019-07-14'
and designated_market_area_id = 632
and rlsd_fr_prcs_flg = "Y"
and intb_flg = "Y"
''')


tuning_dish = spark.sql(''' select * from  tam_stb_igsn_dsh_cloud_prod_dlvry.stb_cnsm_evnt_cppd_with_crdt
where 1=1
and dt_dt between '2019-07-13' and '2019-07-24'
and intb_dt = '2019-07-14'
and designated_market_area_id = 632
and rlsd_fr_prcs_flg = "Y"
and intb_flg = "Y"
''')

tuning = tuning_ch.union(tuning_dtv).union(tuning_dish)
print('number of events',tuning.count())
print('number of distinct housholds',tuning.select('glbl_hh_id').distinct().count())


# COMMAND ----------

# tuning.write.parquet('s3://useast1-nlsn-w-digital-dsci-dev/data/users/pakdmj01/delme/events.parquet',mode = 'overwrite')
event_df = spark.read.parquet('s3://useast1-nlsn-w-digital-dsci-dev/data/users/pakdmj01/delme/events.parquet')

if not all:
  event_df = event_df.filter((F.col('glbl_hh_id') == hhno) & (F.col('nt_st_cd') == stn))
event_df.createOrReplaceTempView('event_df')
print(event_df.cache().count())
# display(event_df)

# COMMAND ----------

display(event_df)

# COMMAND ----------

COL_MAP = {
  'intb_dt' : 'intab_date',
  'viwd_intb_dtdt' : 'viewed_intab_date',
  'glbl_hh_id' : 'global_household_id',
  'dvc_nmbr' : 'source_media_device_number',
  'nt_st_cd':'metered_tv_credit_station_code',
  'credited_sttm_utc':'event_credit_start_utc',
  'credited_endtm_utc' : 'event_credit_end_utc',
  'viewed_sttm_utc' : 'event_start_utc',
  'viewed_endtm_utc' : 'event_end_utc',
}

selected_cols = ['intab_date','viewed_intab_date','global_household_id','metered_tv_credit_station_code','source_media_device_number',
                 'event_credit_start_utc','event_credit_end_utc','event_start_utc','event_end_utc']

event_df = rename_columns(event_df,COL_MAP)
event_df = event_df.select(selected_cols)


event_df = cast_timestamp(event_df, [
        "event_credit_start_utc",
        "event_credit_end_utc",
        "event_start_utc",
        "event_end_utc",    
    ])

display(event_df)

# COMMAND ----------

event_df = event_df.join(household_intab,
                         on = ['global_household_id','intab_date'],
                         how = 'inner'
                         )\
.select(event_df['*'],household_intab['week_number'],household_intab['dma_code'])
  
event_df.count()
display(event_df)
                          

# COMMAND ----------

event_df = duration_in_minutes(event_df,'event_credit_end_utc','event_credit_start_utc','events_duration')
display(event_df)

# COMMAND ----------

event_df.select(F.sum('events_duration')).show()

# COMMAND ----------

event_df = clone_dataframe(event_df)
event_df.cache().count()

# COMMAND ----------

# MAGIC %md
# MAGIC * making daypart could be skiped here. (we need reference files instead of hard coding)
# MAGIC * Can we skip provider pull function completely in this module? does it even matter for rpd and code reader data

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 6 : Program coded table & join with Tuning

# COMMAND ----------

info_df = genre_pull(spark)
info_df.cache().count()

# COMMAND ----------

display(info_df)

# COMMAND ----------

program_code_file_path = 's3://useast1-nlsn-w-digital-dsci-dev/xandr-pgmcoding/version=1/nationalTvBroadcastDate=2019-07-14/'
coded = spark.read.parquet(program_code_file_path) \
.filter('designatedMarketId = 632') 
if not all:
  coded = coded.filter((F.col('globalHouseholdId') == hhno) & (F.col('meteredTvCreditStationCode') == stn))

coded.cache().count()
# display(coded)

# COMMAND ----------

COL_MAP = {
  
  'creditIntabDate' : 'intab_date',
  'viewedIntabDate' : 'viewed_intab_date',
  'globalHouseholdId':'global_household_id',
  'deviceId':'source_media_device_number',
  'meteredTvCreditStationCode' : 'metered_tv_credit_station_code',
  'reportOriginatorLineupKey':'report_originator_lineup_key',
  'creditStartOfViewingUtc':'tuning_credit_start_utc',
  'creditEndOfViewingUtc' : 'tuning_credit_end_utc',
  'startViewingUtcTime' : 'tuning_start_utc',
  'endViewingUtcTime' : 'tuning_end_utc',
  'durationInMinutes' : 'duration_in_minutes'
 
}

selected_columns = ['intab_date','viewed_intab_date','global_household_id','source_media_device_number','metered_tv_credit_station_code',
                   'report_originator_lineup_key','tuning_credit_start_utc','tuning_credit_end_utc','tuning_start_utc','tuning_end_utc',
                   'duration_in_minutes','stream']

coded = rename_columns(coded,COL_MAP)
coded = coded.select(selected_columns)
display(coded)

# COMMAND ----------

coded = convert_timestamp(coded,['tuning_credit_start_utc',
                                 'tuning_credit_end_utc',
                                 'tuning_start_utc',
                                 'tuning_end_utc']
                          )

# COMMAND ----------

coded = coded.join(info_df,
          on = ['report_originator_lineup_key'],
          how = 'inner') \
.select(coded['*'],info_df['program_summary_type_code'],info_df['program_expanded_type_code'],info_df['program_name'])
display(coded)

# COMMAND ----------

# MAGIC %md
# MAGIC * Even local/national donor data requires line up for lpm and diary. So genre pull in Mike's software need to change
# MAGIC * current approach, pull event tables, pull program coded tables and join---future approach pull event table pull lineup table and join, get_coded_event could be removed then
# MAGIC * why there is a join between event table and program coded table here?

# COMMAND ----------

# MAGIC %md
# MAGIC * we might need to check it with Tony 

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 7

# COMMAND ----------

# MAGIC %md
# MAGIC * Can I completely skip make_event_genre (part 7) which takes care of overlaping events? 
# MAGIC * we cannot possible have a contradiction in start time and end time across event table and program coded event table
# MAGIC * even if this is necessary, 128 out of more thatn 2 million rows. is it worth the UDF?
# MAGIC * if no, how to use make_flags functions, I lack most variables

# COMMAND ----------

# MAGIC %md
# MAGIC  
# MAGIC # Part 8 : Join event and program coded tables

# COMMAND ----------

display(event_df.filter('1 = 1').
        select('intab_date','global_household_id','source_media_device_number','event_credit_start_utc','event_credit_end_utc','events_duration')
       .orderBy('event_credit_end_utc'))

# COMMAND ----------

display(coded.filter('1 = 1')
        .select('intab_date','global_household_id','source_media_device_number','tuning_credit_start_utc','tuning_credit_end_utc','duration_in_minutes','program_name')
       .orderBy('tuning_credit_start_utc'))

# COMMAND ----------

event_coded = event_df.join(coded,
            on =['intab_date','global_household_id','metered_tv_credit_station_code','source_media_device_number'
                ],
                 how ='left') \
.where((event_df['event_credit_start_utc']<=coded['tuning_credit_end_utc']) & (event_df['event_credit_end_utc']>=coded['tuning_credit_start_utc'])) \
.select(event_df['*'],coded['tuning_credit_start_utc'],coded['tuning_credit_end_utc'],coded['program_name'])
# .where (tuning2['credited_endtm_utc'].isNull())
# event_coded.createOrReplaceTempView('event_coded')
event_coded.count()

# COMMAND ----------

event_coded = clone_dataframe(event_coded)
event_coded.cache().count()

# COMMAND ----------

display(event_coded.filter('1 = 1') \
        .select('intab_date','global_household_id','source_media_device_number',
                'event_credit_start_utc','event_credit_end_utc','events_duration','tuning_credit_start_utc','tuning_credit_end_utc','program_name')
       .orderBy('tuning_credit_start_utc')
       )

# COMMAND ----------

event_coded = adj_time(event_coded,'event_credit_start_utc','event_credit_end_utc','tuning_credit_start_utc','tuning_credit_end_utc')
event_coded = duration_in_minutes(event_coded,'event_credit_end_utc','event_credit_start_utc','events_duration')
# 

# COMMAND ----------

display(event_coded.filter('1 = 1') \
        .select('intab_date','global_household_id','source_media_device_number',
                'event_credit_start_utc','event_credit_end_utc','events_duration','tuning_credit_start_utc','tuning_credit_end_utc','program_name')
       .orderBy('tuning_credit_start_utc')
       )

# COMMAND ----------

event_coded.select(F.sum('events_duration')).show()

# COMMAND ----------

# event_coded = overlap_type(event_coded,'event_credit_start_utc','event_credit_end_utc','tuning_credit_start_utc','tuning_credit_end_utc')

# display(event_coded.filter('1 = 1') \
#         .select('intab_date','global_household_id','source_media_device_number',
#                 'event_credit_start_utc','event_credit_end_utc','events_duration','tuning_credit_start_utc','tuning_credit_end_utc','program_name','swap')
#        .orderBy('tuning_credit_start_utc')
#        )

# COMMAND ----------

# event_coded = swap_timestamps(event_coded,'event_credit_start_utc','event_credit_end_utc','tuning_credit_start_utc','tuning_credit_end_utc').orderBy('tuning_credit_start_utc')


# display(event_coded.filter('1 = 1') \
#         .select('intab_date','global_household_id','source_media_device_number',
#                 'event_credit_start_utc','event_credit_end_utc','events_duration','tuning_credit_start_utc','tuning_credit_end_utc','program_name')
#        .orderBy('tuning_credit_start_utc')
#        )

# COMMAND ----------

# event_coded.write.partitionBy('intab_date').parquet('s3://useast1-nlsn-w-digital-dsci-dev/data/users/pakdmj01/delme/event_coded.parquet',mode = 'overwrite')
event_coded = spark.read.parquet('s3://useast1-nlsn-w-digital-dsci-dev/data/users/pakdmj01/delme/event_coded.parquet')
event_coded.createOrReplaceTempView('event_coded')
print(event_coded.cache().count())
# display(event_coded)

# COMMAND ----------

# MAGIC %md
# MAGIC * In module 8, the only thing that we require is a join
# MAGIC * can we completely skip fill gap module?  same reason that applied to overlaping evning
# MAGIC * join between events and lineup or program coded table is a range join. does it work for big data?

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 9: Pulling vieweing events

# COMMAND ----------

# MAGIC %md
# MAGIC * skip this part

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 10 : Join ouput of 8 and 9

# COMMAND ----------

# MAGIC %md
# MAGIC * skip this part

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 11 : Expand data by quarter hour : 

# COMMAND ----------

refrence_file = spark.sql( """select * from tam_lpm_mch_tv_prod.dma_time_by_qhr 
where broadcast_date_key between '20190713' and '20190715'""")

refrence_file = refrence_file.withColumnRenamed('meter_intab_date','intab_date').withColumnRenamed('eastern_standard_date_time_of_qhr','est_start_time_of_qhr')
refrence_file = refrence_file.withColumn('est_end_time_of_qhr',timestamp_delta('est_end_time_of_qhr',59))
refrence_file.cache().count()
# display(refrence_file)

# COMMAND ----------

display(refrence_file.select('intab_date','dma_code','quarter_hour_of_week_key','est_start_time_of_qhr','est_end_time_of_qhr'))

# COMMAND ----------

event_coded_expanded = event_coded.join(refrence_file,
            on =['intab_date','dma_code'
                ],
                 how ='inner') \
.where((event_coded['tuning_credit_start_utc']<=refrence_file['est_end_time_of_qhr']) & (event_coded['tuning_credit_end_utc']>=refrence_file['est_start_time_of_qhr'])) \
.select(event_coded['*'],refrence_file['est_start_time_of_qhr'],refrence_file['est_end_time_of_qhr'])
# .where (tuning2['credited_endtm_utc'].isNull())
event_coded_expanded.createOrReplaceTempView('event_coded_expanded')
event_coded_expanded.count()

# COMMAND ----------

display(event_coded_expanded.filter('1 = 1') \
        .select('intab_date','global_household_id','source_media_device_number',
                'event_credit_start_utc','event_credit_end_utc','events_duration','tuning_credit_start_utc','tuning_credit_end_utc','program_name',
               'est_start_time_of_qhr','est_end_time_of_qhr')
       .orderBy('est_start_time_of_qhr')
       )

# COMMAND ----------

event_coded_expanded = adj_time(event_coded_expanded,'event_credit_start_utc','event_credit_end_utc','est_start_time_of_qhr','est_end_time_of_qhr')
event_coded_expanded = duration_in_minutes(event_coded_expanded,'event_credit_end_utc','event_credit_start_utc','events_duration')
# display(event_coded_expanded1)

# COMMAND ----------

display(event_coded_expanded.filter('1 = 1') \
        .select('intab_date','global_household_id','source_media_device_number',
                'event_credit_start_utc','event_credit_end_utc','events_duration','tuning_credit_start_utc','tuning_credit_end_utc','program_name',
               'est_start_time_of_qhr','est_end_time_of_qhr')
       .orderBy('est_start_time_of_qhr'))

# COMMAND ----------

event_coded_expanded.select(F.sum('events_duration')).show()

# COMMAND ----------

# event_coded_expanded1 = overlap_type(event_coded_expanded,'event_credit_start_utc','event_credit_end_utc','est_start_time_of_qhr','tuning_credit_end_utc')

# display(event_coded_expanded1.filter('1 = 1') \
#         .select('intab_date','global_household_id','source_media_device_number',
#                 'event_credit_start_utc','event_credit_end_utc','events_duration','tuning_credit_start_utc','tuning_credit_end_utc','program_name',
#                'est_start_time_of_qhr','est_end_time_of_qhr','swap')
#        .orderBy('est_start_time_of_qhr')
#        )


# event_coded_expanded2 = swap_timestamps(event_coded_expanded1,'event_credit_start_utc','event_credit_end_utc','est_start_time_of_qhr','tuning_credit_end_utc').orderBy('est_start_time_of_qhr')

# display(event_coded_expanded2.filter('1 = 1') \
#         .select('intab_date','global_household_id','source_media_device_number',
#                 'event_credit_start_utc','event_credit_end_utc','events_duration','tuning_credit_start_utc','tuning_credit_end_utc','program_name','swap',
#                'est_start_time_of_qhr','est_end_time_of_qhr')
#        .orderBy('est_start_time_of_qhr')
#        )

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 12: Bring demos

# COMMAND ----------

display(demo_df.filter((F.col('global_household_id') == hhno) ))

# COMMAND ----------

final_df = event_coded_expanded.join(demo_df,
                         on = ['intab_date','global_household_id'],
                         how = 'inner') \
           .select(event_coded_expanded['*'],demo_df['PERSON_NUMBER'],demo_df["AGE"],demo_df["GENDER"])
final_df.count()

# COMMAND ----------

display(final_df.filter('1 = 1') \
        .select('intab_date','global_household_id','source_media_device_number','PERSON_NUMBER','AGE','GENDER',
                'event_credit_start_utc','event_credit_end_utc','events_duration','tuning_credit_start_utc','tuning_credit_end_utc','program_name',
               'est_start_time_of_qhr','est_end_time_of_qhr')
       .orderBy('PERSON_NUMBER','est_start_time_of_qhr'))

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC # Extra modules

# COMMAND ----------

display(event_df)

# COMMAND ----------

display(event_df.filter('1 = 1').
        select('intab_date','global_household_id','source_media_device_number','event_credit_start_utc','event_credit_end_utc','events_duration')
       .orderBy('event_credit_end_utc'))

# COMMAND ----------

lineup = spark.read.parquet('s3://useast1-nlsn-w-digital-dsci-dev/data/users/pakdmj01/delme/nielsen_lineup.parquet')
lineup.cache().count()
display(lineup)

# COMMAND ----------

display(lineup.select('source','report_originator_lineup_key','media_distributor_id','metered_tv_credit_station_code','station_code','program_name','broadcast_start_utc','broadcast_end_utc'))

# COMMAND ----------

event_coded1 = event_df.join(lineup,
             on = [event_df['metered_tv_credit_station_code'] == lineup['station_code']]
                  ) \
.where((event_df['event_credit_start_utc']<=lineup['broadcast_end_utc']) & (event_df['event_credit_end_utc']>=lineup['broadcast_start_utc']))\
.select(event_df['*'],lineup['program_name'],lineup['broadcast_start_utc'],lineup['broadcast_end_utc'])


event_coded1.cache().count()
display(event_coded1)

# COMMAND ----------

display(event_coded1.filter('1 = 1').
        select('intab_date','global_household_id','source_media_device_number','event_credit_start_utc','event_credit_end_utc','events_duration','program_name','broadcast_start_utc','broadcast_end_utc')
       .orderBy('broadcast_start_utc'))

# COMMAND ----------

display(coded.filter('1 = 1')
        .select('intab_date','global_household_id','source_media_device_number','tuning_credit_start_utc','tuning_credit_end_utc','duration_in_minutes','program_name')
       .orderBy('tuning_credit_start_utc'))

# COMMAND ----------


